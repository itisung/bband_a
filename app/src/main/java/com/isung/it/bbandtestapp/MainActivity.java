package com.isung.it.bbandtestapp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.BaseAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private LeDeviceListAdapter deviceListAdapter;
    private BluetoothAdapter btAdapter;
    private Handler mHandler;

    private static final long SCAN_PERIOD = 60000*5;
    private boolean mScanning;

    private ListView lvDeviceList;
    private BLEService bleService;
    private TextView textView2;

    private int whereIam;
    private int deviceRssi;
    private int resetCount;
    private boolean updatePosition;
    private String strPositionName;

    private BLEService leService;

    private Button btnEvt1;
    private Button btnEvt2;
    private Button btnEvt3;

    public static Context ct;

    byte[] value;

    private int notiType;

    private boolean bBtnEvnt1Clicked;
    private boolean bBtnEvnt2Clicked;
    private boolean bBtnEvnt3Clicked;

   // public static UUID UART_UUID = UUID.fromString("0000870c-0000-1000-8000-00805f9b34fb");  //0000870c-0000-1000-8000-00805f9b34fb  //e18c92a4-1ad0-40b1-abcc-d955d0a5998b
    public static UUID UART_UUID   = UUID.fromString("87011111-ffcc-2222-0000-000000008888");

    public BluetoothGattCharacteristic selectedCharacteristic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //startActivity(new Intent(this, SplashActivity.class));

        setContentView(R.layout.activity_main);

        ct = MainActivity.this;

        initActionBar();

        btnEvt1 = (Button)findViewById(R.id.btnEvent1);
        btnEvt2 = (Button)findViewById(R.id.btnEvent2);
        btnEvt3 = (Button)findViewById(R.id.btnEvent3);
        btnEvt1.setOnClickListener( btnScanClick );
        btnEvt2.setOnClickListener( btnScanClick );
        btnEvt3.setOnClickListener( btnScanClick );

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = bluetoothManager.getAdapter();

        if (btAdapter == null) {
            Toast.makeText(this, R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        lvDeviceList = (ListView)findViewById(R.id.listViewDeviceList);

        lvDeviceList.setOnItemClickListener( lvClickListener );

        textView2 = (TextView)findViewById(R.id.textView2);

        mHandler = new Handler();

        whereIam = 0;
        resetCount = 0;
        updatePosition = false;
        deviceRssi = 0;

        Intent gattServiceIntent = new Intent(this, BLEService.class);
        bindService(gattServiceIntent, serviceConnection, BIND_AUTO_CREATE);

        value = new byte[2];

        notiType = 0;


        bBtnEvnt1Clicked = false;
        bBtnEvnt2Clicked = false;
        bBtnEvnt3Clicked = false;

        updateButtonState();

        SharedPreferences activityPrefs = BLEApiUtils.getPreferences(this);
        boolean mFirstRun = activityPrefs.getBoolean(Preferences.SETTING_FIRST_RUN, true);

        Log.d(TAG, "mFirstRun = " + mFirstRun);
        SharedPreferences.Editor editor = activityPrefs.edit();
        editor.putBoolean(Preferences.SETTING_FIRST_RUN, false);
        editor.commit();

        //test code
//        passActivityTestCode();
    }

    private void passActivityTestCode(){
        final Intent intent = new Intent(MainActivity.this, DeviceActivity.class);
        intent.putExtra(DeviceActivity.EXTRAS_DEVICE_NAME, "test device");
        intent.putExtra(DeviceActivity.EXTRAS_DEVICE_ADDRESS, "00:02:5B:00:15:10");
        if (mScanning) {
            btAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }
        startActivityForResult(intent, 0);
    }

    private void initActionBar() {
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setBackgroundDrawable( getResources().getDrawable(R.color.action_bar_color) );
    }

    @Override
    protected void onResume() {
        super.onResume();

        deviceListAdapter = new LeDeviceListAdapter();
        lvDeviceList.setAdapter(deviceListAdapter);

        Log.d("mymy", "click button scan");
        scanLeDevice(true);
    }

    private void updateButtonState() {
        if( bBtnEvnt1Clicked == false ) {
            btnEvt1.setText("전화왔음");
        }
        else {
            btnEvt1.setText("전화확인");

            final Intent intent = new Intent(MainActivity.this, DeviceActivity.class);
            intent.putExtra(DeviceActivity.EXTRAS_DEVICE_NAME, "test device");
            intent.putExtra(DeviceActivity.EXTRAS_DEVICE_ADDRESS, "00:02:5B:00:15:10");
            if (mScanning) {
                btAdapter.stopLeScan(mLeScanCallback);
                mScanning = false;
            }
            startActivityForResult(intent, 0);
        }

        if( bBtnEvnt2Clicked == false ) {
            btnEvt2.setText("문자왔음");
        }
        else {
            btnEvt2.setText("문자확인");
        }

        if( bBtnEvnt3Clicked == false ) {
            btnEvt3.setText("카톡왔음");
        }
        else {
            btnEvt3.setText("카톡확인");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        finish();
    }

    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            leService = ((BLEService.LocalBinder) service).getService();
            if (!leService.initialize()) {
                Log.d("mymy", "Unable to initialize Bluetooth");
                finish();
            }

            leService.registerCallback( mCallback );
            // Automatically connects to the device upon successful start-up
            // initialization.
            //leService.connect(btAddress);

            Log.d("mymy", "Service Connected, MainActivity");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            leService = null;

            Log.d("mymy", "Service Disconnected, MainActivity");
        }

        private BLEService.ICallback mCallback = new BLEService.ICallback() {
            public void sendData(String msg) {
                Log.d("mymy", "MainActivity, Service Callback : " + msg);

                textView2.setText(msg);
            }
        };
    };

    View.OnClickListener btnScanClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch( v.getId() ) {
                case R.id.btnEvent1 :
                    if( bBtnEvnt1Clicked == true ) {
                        bBtnEvnt1Clicked = false;
                        sendNotiCmd( 1, false );
                    }
                    else {
                        bBtnEvnt1Clicked = true;
                        sendNotiCmd( 1, true );
                    }

                    updateButtonState();
                    break;
                case R.id.btnEvent2 :
                    if( bBtnEvnt2Clicked == true ) {
                        bBtnEvnt2Clicked = false;
                        sendNotiCmd( 2, false );
                    }
                    else {
                        bBtnEvnt2Clicked = true;
                        sendNotiCmd( 2, true );
                    }

                    updateButtonState();
                    break;
                case R.id.btnEvent3 :
                    if( bBtnEvnt3Clicked == true ) {
                        bBtnEvnt3Clicked = false;
                        sendNotiCmd( 4, false );
                    }
                    else {
                        bBtnEvnt3Clicked = true;
                        sendNotiCmd( 4, true );
                    }

                    updateButtonState();
                    break;
            }
        }
    };

    public void testTest() {

    }

    private void sendNotiCmd(int whatClick, boolean bOn) {
        if(bOn == true) {
            notiType = notiType + whatClick;
        }
        else {
            notiType = notiType - whatClick;
        }

        Log.d("mymy", "clicked noti *** : " + notiType);

        value[0] = 0x01;
        value[1] = (byte)(notiType & 0xFF);

        selectedCharacteristic.setValue(value);
        leService.writeCharacteristic(selectedCharacteristic);
    }

    private AdapterView.OnItemClickListener lvClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String strData = "onClick : " + position;
            Log.d("mymy", strData);

            final BluetoothDevice device = deviceListAdapter.getDevice(position);
            if (device == null) {
                Log.d("mymy", "onClick : if (device == null)");
                return;
            }
            final Intent intent = new Intent(MainActivity.this, DeviceActivity.class);
            intent.putExtra(DeviceActivity.EXTRAS_DEVICE_NAME,
                    device.getName());
            intent.putExtra(DeviceActivity.EXTRAS_DEVICE_ADDRESS,
                    device.getAddress());
            if (mScanning) {
                btAdapter.stopLeScan(mLeScanCallback);
                mScanning = false;
            }

            MyApplication myApp = (MyApplication) getApplication();
            myApp.setGlobalBluetoothDevice(device);

            startActivityForResult(intent, 0);
        }
    };

    @Override
    protected void onPause() {
        super.onPause();

        scanLeDevice(false);
        deviceListAdapter.clear();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unbindService(serviceConnection);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        TextView deviceState;
        TextView rssi;
    }

    private class ScanInfo implements Comparable<ScanInfo> {
        public String name;
        public String address;
        public int rssi;
        public String state;

        public ScanInfo(String name, String address, int rssi, String state) {
            this.name = name;
            this.address = address;
            this.rssi = rssi;
            this.state = state;
        }

        @Override
        public int compareTo(ScanInfo another) {
            final int BEFORE = -1;
            final int EQUAL = 0;
            final int AFTER = 1;

            if (rssi == another.rssi)
                return EQUAL;
            if (this.rssi < another.rssi)
                return AFTER;
            if (this.rssi > another.rssi)
                return BEFORE;

            return EQUAL;
        }
    }

    Runnable listUpdateRunnable = new Runnable() {

        @Override
        public void run() {
            /*
            ListView lv = getListView();

            if( lv != null )
            {
                if(lv.getCount() > 1)
                {
                    lv.setSelection(lv.getCount() -1);
                }
            }

            mHandler.postDelayed(listUpdateRunnable, 500);
            */
        }

    };

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    btAdapter.stopLeScan(mLeScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            btAdapter.startLeScan(mLeScanCallback);
        }
        else {
            mScanning = false;
            btAdapter.stopLeScan(mLeScanCallback);
        }

        invalidateOptionsMenu();
    }

    /*
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<ScanInfo> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<ScanInfo>();
            mInflator = MainActivity.this.getLayoutInflater();
        }

        public void addDevice(ScanInfo device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;

            Log.d("mymy", "geView");

            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.deviceState = (TextView) view.findViewById(R.id.device_state);
                viewHolder.rssi = (TextView) view.findViewById(R.id.device_rssi);

                view.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder) view.getTag();
            }

            ScanInfo device = mLeDevices.get(i);

            final String deviceName = device.name;

            if (deviceName != null && deviceName.length() > 0) {
                viewHolder.deviceName.setText(deviceName);
            }
            else {
                viewHolder.deviceName.setText("Module");
            }

            viewHolder.deviceAddress.setText(device.address);
            viewHolder.deviceState.setText(device.state);

            String testString = Integer.toString(device.rssi);

            viewHolder.rssi.setText(testString);
           // mHandler.post(listUpdateRunnable);

            return view;
        }
    }
    */

    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = MainActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.rssi = (TextView) view.findViewById(R.id.device_rssi);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0) {
                viewHolder.deviceName.setText(deviceName);
            }
            else {
                viewHolder.deviceName.setText(R.string.unknown_device);
            }

            viewHolder.deviceAddress.setText(device.getAddress());
            //viewHolder.rssi.setText();

            return view;
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String name = device.getName();

                    //device.getBondState();

                    Log.d("mymy", "onLeScan run, name : " + device.getName() + ", " + device.getAddress() + ", " + rssi +
                            ", " + device.getBondState() + ", " + device.getType());


                    //TODO JSLEE 임시 제거
//                    if( name.equals("AirPowerManager")) {
                        deviceListAdapter.addDevice(device);
                        deviceListAdapter.notifyDataSetChanged();
//                    }


                    /*
                    if( name.equals("CSR Serial Server")) {


                        byte[] advertisedData = Arrays.copyOf(scanRecord, scanRecord.length);

                        Log.d("mymy", "okok");
                    }
                    */

                    /*
                    if( device.getBondState() == 12 ) {
                        Log.d("mymy", "already bonede device found.");

                        //final BluetoothDevice device = deviceListAdapter.getDevice(position);
                        if (device == null)
                            return;
                        final Intent intent = new Intent(MainActivity.this, DeviceActivity.class);
                        intent.putExtra(DeviceActivity.EXTRAS_DEVICE_NAME,
                                device.getName());
                        intent.putExtra(DeviceActivity.EXTRAS_DEVICE_ADDRESS,
                                device.getAddress());
                        if (mScanning) {
                            btAdapter.stopLeScan(mLeScanCallback);
                            mScanning = false;
                        }
                        startActivity(intent);
                    }
                    */



                    //byte[] advertisedData = Arrays.copyOf(scanRecord, scanRecord.length);

                    List<UUID> scannedUUIDs = uuidsFromAdvert(scanRecord);

                    if (parseUUIDs(scanRecord).contains(UART_UUID)) {
                        Log.d("mymy", "It's ok...ok...ok");

                        textView2.setText("BBand가 검색되었습니다.");
                    }

                    /*
                    boolean continueParsing = false;

                    // 허용된 bt address를 체크하기 위함.
                    // 어드레스를 :로 잘라서 마지막을 체크해서 허용된 address인가 확인한다.
                    String[] btAddr = device.getAddress().split(":");

                    Log.d("mymy", "onLeScan run, type : " + btAddr[5]);

                    if( btAddr[5].equals("99") || btAddr[5].equals("98") || btAddr[5].equals("97")) {
                        continueParsing = true;
                    }

                    if( continueParsing == true ) {
                        String state = null;
                        StringBuilder sb = new StringBuilder();

                        // ble scan은 앱에서 계속한다.
                        // 그런데 쉬지않고 바로바로 갱신을 하면 검색된 정보가 시시각각 앱에서
                        // 보여지기 때문에 조잡해보인다.
                        // 따라서 bt scan을 5번을 하고 5번마다 한번씩 화면에 보여주도록 하자.

                        resetCount = resetCount + 1;

                        for (byte b : scanRecord) {
                            sb.append(String.format("%02X ", b));
                        }

                        Log.d("mymy", "onLeScan run, data : " + scanRecord[7]);

                        // 5번 scan을 무시하는 동안, rssi값이 제일좋은 놈을 계속 갱신해서
                        // 가지고 있도록 한다.
                        // 최종적으로 5번이 되었을때 마지막으로 가지고 있던 값을
                        // 앱에서 보여주게 된다.
                        if( deviceRssi == 0 ) {
                            //whereIam = Integer.parseInt( scanRecord[7] );
                            deviceRssi = Math.abs( rssi );
                            updatePosition = true;
                        }
                        else if( deviceRssi > Math.abs(rssi)) {
                            deviceRssi = Math.abs( rssi );
                            updatePosition = true;
                        }


                        if( updatePosition == true ) {
                            updatePosition = false;

                            if (scanRecord[7] == 0x01) {
                                strPositionName = "음식점";
                                //textView2.setText("음식점");
                            }
                            if (scanRecord[7] == 0x02) {
                                strPositionName = "커피전문점";
                                //textView2.setText("커피전문점");
                            }
                            else if (scanRecord[7] == 0x03) {
                                strPositionName = "의류매장";
                                //textView2.setText("의류매장");
                            }
                        }

                        // 5번동안 rssi를 체크하면서 가장 감도 좋은 놈을 가지고 있다가
                        // 앱에서 보여주고, 모든 값을 초기화함.
                        if( resetCount >= 5 ) {
                            deviceRssi = 200;
                            resetCount = 0;
                            textView2.setText( strPositionName );
                        }
                        */


                    /*
                    ScanInfo scanResult = new ScanInfo(device.getName(),
                            device.getAddress(), rssi, state);
                            */


                  //  }
                }
            });
        }
    };

    private List<UUID> parseUUIDs(final byte[] advertisedData) {
        List<UUID> uuids = new ArrayList<UUID>();

        int offset = 0;
        while (offset < (advertisedData.length - 2)) {
            int len = advertisedData[offset++];
            if (len == 0)
                break;

            int type = advertisedData[offset++];
            switch (type) {
                case 0x02: // Partial list of 16-bit UUIDs
                case 0x03: // Complete list of 16-bit UUIDs
                    while (len > 1) {
                        int uuid16 = advertisedData[offset++];
                        uuid16 += (advertisedData[offset++] << 8);
                        len -= 2;
                        uuids.add(UUID.fromString(String.format("%08x-0000-1000-8000-00805f9b34fb", uuid16)));
                    }
                    break;
                case 0x06:// Partial list of 128-bit UUIDs
                case 0x07:// Complete list of 128-bit UUIDs
                    // Loop through the advertised 128-bit UUID's.
                    while (len >= 16) {
                        try {
                            // Wrap the advertised bits and order them.
                            ByteBuffer buffer = ByteBuffer.wrap(advertisedData, offset++, 16).order(ByteOrder.LITTLE_ENDIAN);
                            long mostSignificantBit = buffer.getLong();
                            long leastSignificantBit = buffer.getLong();
                            uuids.add(new UUID(leastSignificantBit,
                                    mostSignificantBit));
                        } catch (IndexOutOfBoundsException e) {
                            // Defensive programming.
                            //Log.e(LOG_TAG, e.toString());
                            continue;
                        } finally {
                            // Move the offset to read the next uuid.
                            offset += 15;
                            len -= 16;
                        }
                    }
                    break;
                default:
                    offset += (len - 1);
                    break;
            }
        }
        return uuids;
    }

    /*
    funnylogic - test code 삭제할것
    public static boolean hasMyService(byte[] scanRecord) {

        // UUID we want to filter by (without hyphens)
        final String myServiceID = "0000000000001000800000805F9B34FB";

        // The offset in the scan record. In my case the offset was 13; it will probably be different for you
        final int serviceOffset = 13;

        try{

            // Get a 16-byte array of what may or may not be the service we're filtering for
            byte[] service = ArrayUtils.subarray(scanRecord, serviceOffset, serviceOffset + 16);

            // The bytes are probably in reverse order, so we need to fix that
            ArrayUtils.reverse(service);

            // Get the hex string
            String discoveredServiceID = bytesToHex(service);

            // Compare against our service
            return myServiceID.equals(discoveredServiceID);

        } catch (Exception e){
            return false;
        }

    }
    */

    private List<UUID> uuidsFromAdvert(final byte[] advData) {
        final List<UUID> uuids = new ArrayList<UUID>();

        // Pointer within advData to the start of the current ad element being processed.
        int ptrToAdElement = 0;

        // Offsets from start of ad element (i.e. from ptrToAdElement).
        final int OFFSET_LENGTH = 0;
        final int OFFSET_TYPE = 1;
        final int OFFSET_DATA = 2;

        final byte AD_TYPE_UUID_16BIT = 0x02;
        final byte AD_TYPE_UUID_16BIT_LIST = 0x03;
        final byte AD_TYPE_UUID_128BIT = 0x06;
        final byte AD_TYPE_UUID_128BIT_LIST = 0x07;

        final int UUID_16_LENGTH = 2;
        final int UUID_128_LENGTH = 16;

        final String BASE_UUID_FORMAT = "%08x-0000-1000-8000-00805f9b34fb";

        while (ptrToAdElement < advData.length - 1) {
            final byte length = advData[ptrToAdElement + OFFSET_LENGTH];

            // The advert data returned by the Android API is padded out with trailing zeroes, so if we reach a
            // zero length then we are done.
            if (length == 0)
                break;

            // Check that there is enough remaining data in the advert for the indicated length.
            if (length > (advData.length - ptrToAdElement - 1)) {
                // This was a malformed advert so return an empty list, even if we got some UUIDs already.
                uuids.clear();
                return uuids;
            }

            final byte adType = advData[ptrToAdElement + OFFSET_TYPE];

            switch (adType) {
                case AD_TYPE_UUID_16BIT:
                case AD_TYPE_UUID_16BIT_LIST:
                    for (int i = length; i > UUID_16_LENGTH - 1; i -= UUID_16_LENGTH) {
                        int uuid16 = (advData[ptrToAdElement + OFFSET_DATA] & 0xFF);
                        uuid16 |= ((advData[ptrToAdElement + OFFSET_DATA + 1] & 0xFF) << 8);
                        uuids.add(UUID.fromString(String.format(BASE_UUID_FORMAT, uuid16)));
                    }
                    break;
                case AD_TYPE_UUID_128BIT:
                case AD_TYPE_UUID_128BIT_LIST:
                    for (int i = length; i > UUID_128_LENGTH - 1; i -= UUID_128_LENGTH) {
                        long msb = ((advData[ptrToAdElement + OFFSET_DATA] & 0xFF) << 56);
                        msb |= ((advData[ptrToAdElement + OFFSET_DATA + 1] & 0xFF) << 48);
                        msb |= ((advData[ptrToAdElement + OFFSET_DATA + 2] & 0xFF) << 40);
                        msb |= ((advData[ptrToAdElement + OFFSET_DATA + 3] & 0xFF) << 32);
                        msb |= ((advData[ptrToAdElement + OFFSET_DATA + 4] & 0xFF) << 24);
                        msb |= ((advData[ptrToAdElement + OFFSET_DATA + 5] & 0xFF) << 16);
                        msb |= ((advData[ptrToAdElement + OFFSET_DATA + 6] & 0xFF) << 8);
                        msb |= ((advData[ptrToAdElement + OFFSET_DATA + 7] & 0xFF) << 0);
                        long lsb = ((advData[ptrToAdElement + OFFSET_DATA + 8] & 0xFF) << 56);
                        lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 9] & 0xFF) << 48);
                        lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 10] & 0xFF) << 40);
                        lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 11] & 0xFF) << 32);
                        lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 12] & 0xFF) << 24);
                        lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 13] & 0xFF) << 16);
                        lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 14] & 0xFF) << 8);
                        lsb |= ((advData[ptrToAdElement + OFFSET_DATA + 15] & 0xFF) << 0);
                        uuids.add(new UUID(msb, lsb));
                    }
                    break;
                default:
                    // An advert type we don't care about.
                    break;
            }

            // Length byte isn't included in length, hence the +1.
            ptrToAdElement += length + 1;
        }

        return uuids;
    }
}
