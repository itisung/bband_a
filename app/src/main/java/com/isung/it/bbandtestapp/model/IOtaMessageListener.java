/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

import com.isung.it.bbandtestapp.model.State.OtaState;

public interface IOtaMessageListener {

    /**
     * This method is called when the listener is receiving Bluetooth address from the remote device
     * 
     * @param btAddress
     *            String of the remote device's Bluetooth address
     */
    public void onBtAddressUpdate(String btAddress);

    /**
     * This method is called when the listener is receiving Crystal trim from the remote device
     * 
     * @param value
     *            int the value of remote device's crystal trim
     * 
     */
    public void onCrystalTrimUpdate(int value);

    /**
     * This method is called when the listener is receiving 32-bit Identity Root value
     * 
     * @param value
     *            String the value of remote device's identity root
     */
    public void onIdentityRootUpdate(String value);

    /**
     * This method is called when the listener is receiving Encryption Root from the remote device
     * 
     * @param value
     *            String the value of remote device's encryption root
     * 
     */
    public void onEncryptionRootUpdate(String value);

    /**
     * This method is called when the listener is receiving Bootloader version from the remote device
     * 
     * @param version
     *            String remote device's Bootloader version
     */
    public void onBootloaderVersionUpdate(String version);

    /**
     * This method is called when the listener is receiving CSR OTA version from the remote device
     *
     * @param version
     *            String remote device's CSR OTA version
     */
    public void onCsrOtaVersionUpdate(String version);

    /**
     * This method is called when the listener is receiving Bootloader software version from the remote device
     * It refers to the version of bootloader software on the device.
     * Different versions of bootloader software may support the same protocol version.
     * It was introduced because sometimes, particularly during development,
     * it is useful to know which build of the bootloader is present on the device.
     * The Software Version is required to uniquely identify the bootloader.
     * This version number is expressed as an array of uint8 containing an ASCII encoded string, currently “6.0.0.1”.
     *
     * @param version
     *            String remote device's Bootloader software version
     */
    public void onSoftwareVersionUpdate(String version);

    /**
     * This method is called when the listener is receiving software version from device information service (UUID-180A)
     *
     * @param version
     *            String software version
     */
    public void onApplicationVersionUpdate(String version);

    /**
     * This method is called when an error is happened during reading/writing characteristic
     * 
     * @param errorMessageId
     *            int The error message id when reading/writing characteristic
     * 
     */
    public void onOtaMessageUpdate(int errorMessageId);

    /**
     * This method is called when BLE connection is updated
     * 
     * @param state
     *            int the state of BLE connection
     * 
     */
    public void onConnectionStateUpdate(int state);

    /**
     * This method is called when the remote device's bond status is updated
     * 
     * @param state
     *            int the state of Bond stutus
     */
    public void onBondStateUpdate(int state);

    /**
     * This method is called when downloading progress is updated
     * 
     * @param progress
     *            int progress the new progress, between 0 and 100
     * 
     * @param size
     *            int the size of total image data
     * 
     * @param sentNumber
     *            int the number of data sent
     * 
     */
    public void onOtaProgressUpdate(int progress, int size, int sentNumber);

    /**
     * This method is called when OTA state is updated
     * 
     * @param state
     *            int OTA state
     */
    public void onOtaStateUpdate(OtaState state);

    /**
     * This method is called when OTA transfer control status is updated
     * 
     * @param state
     *            short the transfer control state
     */
    public void onOtaTransferControlStatus(short state);

    /**
     * This method is called when CS Block data is received
     * 
     * Boot-loader V5 use CS Block characteristic to obtain remote device's CS keys
     * 
     * @param onCsBlockData
     *            int The reading CS Keys offset
     * 
     * @param data
     *            byte[] CS Block data
     */
    public void onCsBlockData(byte[] data);
    
    
    /**
     * This method inform uplayer OTAU procedures is enabled now
     * 
     * 
     */
     public void onOtauEnabled(boolean enabled);
     
     /**
      * This method inform uplayer OTAU procedures is enabled now
      * 
      * 
      */
      public void onOtauStarted();

}
