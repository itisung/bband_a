package com.isung.it.bbandtestapp;

/**
 * Created by leejs on 2015-12-22.
 */
public class Preferences {

    public static final String SETTING_FIRST_RUN = "first_run";
    public static final String SETTING_ON_TIME_HOUR = "on_time_hour";
    public static final String SETTING_ON_TIME_MIN = "on_time_min";
    public static final String SETTING_OFF_TIME_HOUR = "off_time_hour";
    public static final String SETTING_OFF_TIME_MIN = "off_time_min";
    public static final String SETTING_IS_TIME_RESERVATION_USE = "is_time_reservation_use ";
}
