/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CsKeyXmlParser {

    public static final String CSKEY_XML_FILE = Environment.getExternalStorageDirectory().getPath()
            + "/csr/cskey_db.xml";

    /**
     * This class parses XML from CSR uEnergy SDK cskey_db.xml It returns a List of entries,where each list element
     * represents a single Pskey entry.
     * 
     * @param in
     *            InputStream XML input stream
     * 
     * @return List<PskeyEntry> All the Pskey entries
     * 
     * @throws XmlPullParserException
     * @throws IOException
     */
    public List<CskeyItem> parse(InputStream in, int buildId) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readPskeysEntry(parser, buildId);
        }
        finally {
            in.close();
        }
    }

    /**
     * Read Pskey entries by build id It returns a List of entries
     * 
     * @param parser
     *            XmlPullParser XML parser instance
     * 
     * @param buildid
     *            int The build id in cskey_db.xml from uEnergy SDK 2.4 above
     * 
     * @return List<PskeyEntry> All the Pskey entries
     * 
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List<CskeyItem> readPskeysEntry(XmlPullParser parser, int buildId) throws XmlPullParserException,
            IOException {
        List<CskeyItem> entries = new ArrayList<CskeyItem>();
        parser.require(XmlPullParser.START_TAG, null, "PSKEY_DATABASE");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("PSKEYS")) {
                String key = parser.getAttributeName(0);
                if (key.equalsIgnoreCase("BUILD_ID")) {
                    int pskeyBuildId = Integer.parseInt(parser.getAttributeValue(0));
                    if (buildId == pskeyBuildId) {
                        List<CskeyItem> rtnList = readPskeyEntries(parser);
                        if (rtnList.size() > 0)
                            entries.addAll(rtnList);
                    }
                    else
                        skip(parser);
                }
            }
            else {
                skip(parser);
            }
        }
        return entries;
    }

    /**
     * Read all Pskey attributes by PSKEY
     * 
     * @param parser
     *            XmlPullParser XML parser instance
     * 
     * @return List<PskeyEntry> the Pskey entries
     * 
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List<CskeyItem> readPskeyEntries(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<CskeyItem> entries = new ArrayList<CskeyItem>();
        parser.require(XmlPullParser.START_TAG, null, "PSKEYS");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String entryName = parser.getName();
            // Starts by looking for the entry tag
            if (entryName.equals("PSKEY")) {
                String name = null;
                int id = -1;
                int offset = -1;
                int length = -1;
                int attrSize = parser.getAttributeCount();
                if (attrSize >= 4) {
                    for (int i = 0; i < attrSize; i++) {
                        String attName = parser.getAttributeName(i);
                        if (attName.equalsIgnoreCase("NAME")) {
                            name = parser.getAttributeValue(i);
                        }
                        else if (attName.equalsIgnoreCase("ID")) {
                            id = Integer.parseInt(parser.getAttributeValue(i));
                        }
                        else if (attName.equalsIgnoreCase("OFFSET")) {
                            offset = Integer.parseInt(parser.getAttributeValue(i));
                        }
                        else if (attName.equalsIgnoreCase("LENGTH")) {
                            length = Integer.parseInt(parser.getAttributeValue(i));
                        }
                    }
                    CskeyItem entry = new CskeyItem(name, id, offset, length);
                    entries.add(entry);
                    Log.d("JSLEE","11 name = " + entry.name + " value = " + entry.value );

                    for(int a : entry.value){
                        Log.d("JSLEE","this.value = " + a);
                    }

                    skip(parser);
                }
            }
            else {
                skip(parser);
            }
        }
        return entries;
    }

    /**
     * Skips tags the parser isn't interested in. Uses depth to handle nested tags. i.e., if the next tag after a
     * START_TAG isn't a matching END_TAG, it keeps going until it finds the matching END_TAG (as indicated by the value
     * of "depth" being 0).
     * 
     * @param parser
     *            XmlPullParser the parsing functionality
     * @throws XmlPullParserException
     * @throws IOException
     */
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                depth--;
                break;
            case XmlPullParser.START_TAG:
                depth++;
                break;
            }
        }
    }
}
