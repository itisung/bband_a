package com.isung.it.bbandtestapp;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.List;
import java.util.UUID;

public class BLEService extends Service {
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private String deviceAddress;
    private BluetoothGatt btGatt;

    public final static String ACTION_GATT_CONNECTED = "com.isung.example.ble.scan0000.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "com.isung.example.ble.scan0000.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.isung.example.ble.scan0000.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE = "com.isung.example.ble.scan0000.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = "com.isung.example.ble.scan0000.EXTRA_DATA";
    public final static String ACTION_RECEVE_DATA = "com.isung.example.ble.scan0000.ACTION_RECEVE_DATA";

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    private int btDeviceConnectionState = STATE_DISCONNECTED;

    public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID.fromString(GattAttributes.HEART_RATE_MEASUREMENT);

    public interface ICallback {
        public void sendData(String msg);
    }

    private ICallback iCallBack;

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                btDeviceConnectionState = STATE_CONNECTED;
                broadcastUpdate(intentAction);

                Log.d("mymy", "Connected to GATT Server. jslee");
                Log.d("mymy", "Attempting to start service discovery:" + btGatt.discoverServices());

                //			((MainActivity))

                if (iCallBack != null) {
                    //iCallBack.sendData("연결되었습니다.");
                }

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d("mymy", "Disconnected from GATT Server. jslee");
                btDeviceConnectionState = STATE_DISCONNECTED;
                intentAction = ACTION_GATT_DISCONNECTED;
                broadcastUpdate(intentAction);

                if (iCallBack != null) {
                    //iCallBack.sendData("연결이 끊겼습니다.");
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("mymy", "gatt service discovered jslee");
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);

//				btGatt.setCharacteristicNotification(getSupportedGattServices().getCharacteristics, true);
            } else {
                Log.d("mymy", "gatt service discovered fail : " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.d("mymy", "onCharacteristicRead jslee");

            String strUuid = characteristic.getUuid().toString();
            Log.d("mymy", "onCharacteristicRead, uuid : " + strUuid);

            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.d("mymy", "onCharacteristicChanged jslee");

            String strUuid = characteristic.getUuid().toString();

            Log.d("mymy", "onCharacteristicChanged, uuid : " + strUuid);

            final byte[] data = characteristic.getValue();

            StringBuilder sb = new StringBuilder(data.length * 2);
            for( byte b : data) {
                sb.append(String.format("%02x", b & 0xff)).append(" ");
            }
            Log.d("jslee", "data : " + sb);

            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
            int flag = characteristic.getProperties();
            int format = -1;
            if ((flag & 0x01) != 0) {
                format = BluetoothGattCharacteristic.FORMAT_UINT16;
                Log.d("mymy", "Heart rate format UINT16.");
            } else {
                format = BluetoothGattCharacteristic.FORMAT_UINT8;
                Log.d("mymy", "Heart rate format UINT8.");
            }
            final int heartRate = characteristic.getIntValue(format, 1);
            Log.d("mymy", String.format("Received heart rate: %d", heartRate));
            intent.putExtra(EXTRA_DATA, String.valueOf(heartRate));
        } else {
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();

            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);

				/*
                for (byte byteChar : data)
					stringBuilder.append(String.format("%02X ", byteChar));

				intent.putExtra(EXTRA_DATA, new String(data) + "\n"+ stringBuilder.toString());
				*/

                intent.putExtra(ACTION_RECEVE_DATA, data);
//                intent.putExtra(EXTRA_DATA, new String(data));
            }
        }
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        BLEService getService() {
            return BLEService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    public boolean initialize() {
        if (btManager == null) {
            btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

            if (btManager == null) {
                Log.d("mymy", "Unable to initialize BTManager");
                return false;
            }
        }

        btAdapter = btManager.getAdapter();

        if (btAdapter == null) {
            Log.d("mymy", "Unable to obtain a BTAdapter");
            return false;
        }

        return true;
    }

    public void registerCallback(ICallback cb) {
        iCallBack = cb;
        Log.d("mymy", "BLEService, registerCallback ~~");
    }

    public boolean connect(final String address) {
        if (btAdapter == null || address == null) {
            Log.d("mymy", "BLEService, connect, BTAdapter or BTAddress has a problem.");
            return false;
        }

        if (deviceAddress != null && address.equals(deviceAddress) && btGatt != null) {
            if (btGatt.connect()) {
                btDeviceConnectionState = STATE_CONNECTING;
                Log.d("mymy", "BLEService, Gatt connection success");
                return true;
            } else {
                Log.d("mymy", "BLEService, Gatt connection fail");
                return false;
            }
        } else {
            Log.d("mymy", "BLEService, connect, Other problem.");
        }

        final BluetoothDevice device = btAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.d("mymy", "Device not found.");
            return false;
        }

        btGatt = device.connectGatt(this, false, mGattCallback);
        deviceAddress = address;
        btDeviceConnectionState = STATE_CONNECTING;
        return true;
    }

    public void disconnect() {
        if (btAdapter == null || btGatt == null) {
            Log.d("mymy", "BLEService, disconnect, BTAdapter or Gatt has a problem.");
            return;
        }
        Log.d("JSLEE", "BLEService, btGatt.disconnect();");
        btGatt.disconnect();
    }

    public void close() {
        if (btGatt == null) {
            return;
        }

        btGatt.close();
        btGatt = null;
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (btAdapter == null || btGatt == null) {
            Log.d("mymy", "BLEService, readCharacteristic, BTAdapter or Gatt has a problem.");
            return;
        }

        btGatt.readCharacteristic(characteristic);
    }

    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        Log.d("mymy", "LEService, writeCharacteristic");
        if (btAdapter == null || btGatt == null) {
            Log.d("mymy", "BLEService, writeCharacteristic, BTAdapter or Gatt has a problem.");
            return;
        }

        btGatt.writeCharacteristic(characteristic);
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (btAdapter == null || btGatt == null) {
            Log.d("mymy", "BLEService, setCharacteristicNotification, BTAdapter or Gatt has a problem.");
            return;
        }

        btGatt.setCharacteristicNotification(characteristic, enabled);
    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (btGatt == null)
            return null;

        return btGatt.getServices();
    }
}
