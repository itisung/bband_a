/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.csr.btsmart.BtSmartService;

import java.lang.ref.WeakReference;

class OtaMessageHandler {

    private SmHandler mSmHandler;

    /**
     * Initialise state machine
     * 
     */
    private SmHandler initStateMachine(Looper looper) {
        mSmHandler = new SmHandler(looper);
        return mSmHandler;
    }

    /**
     * Constructor creates a StateMachine in main thread.
     * 
     * @param name
     *            The name of the state machine
     */
    protected OtaMessageHandler(String name) {
        Looper looper = Looper.getMainLooper();
        initStateMachine(looper);
    }

    /**
     * @return Handler, maybe null if state machine has quit.
     */
    public final Handler getHandler() {
        return mSmHandler;
    }

    private static class SmHandler extends Handler {

        /**
         * Constructor
         * 
         * @param looper
         *            for dispatching messages
         */
        private SmHandler(Looper looper) {
            super(looper);
        }

        byte[] csblockNotificationValue = null;

        @Override
        public final void handleMessage(Message msg) {
            WeakReference<OtaUpdateManager> OtaManagerweakRef =
                    new WeakReference<OtaUpdateManager>(OtaUpdateManager.getInstance());
            IOtaMessageListener listener = null;
            if (OtaManagerweakRef.get() != null)
                listener = OtaManagerweakRef.get().getListener();

            int requestId = msg.getData().getInt(BtSmartService.EXTRA_CLIENT_REQUEST_ID);
            switch (msg.what) {
            case BtSmartService.MESSAGE_CHARACTERISTIC_VALUE: {
                if (listener != null) {
                    switch (requestId) {
                        case OtaUpdateManager.REQUEST_READ_CSR_OTA_VERSION:
                            byte[] version = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                            listener.onCsrOtaVersionUpdate(""+version[0]);
                            break;
                        case OtaUpdateManager.REQUEST_READ_BL_SOFTWARE_VERSION:
                            version = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                            listener.onSoftwareVersionUpdate(new String(version));
                            break;
                        case OtaUpdateManager.REQUEST_READ_BOOTLOADER_VERSION:
                            version = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                            listener.onBootloaderVersionUpdate(String.format("%d", version[0]));
                            break;
                        case OtaUpdateManager.REQUEST_READ_DEVICE_SOFTWARE_VERSION:
                            version = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                            listener.onApplicationVersionUpdate(new String(version));
                            break;
                        case OtaUpdateManager.REQUEST_READ_DATA_TRANSFER:
                            // Handle reading CS keys from Bootloader V5
                            if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_BT_ADDRES_FROM_BL) {
                                byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                                String address = "";
                                for (int i = value.length - 1; i >= 0; i--) {
                                    address += String.format("%02X:", value[i]);
                                }
                                String ret = address.substring(0, address.length() - 1);
                                listener.onBtAddressUpdate(ret);
                            }
                            else if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_XTAL_TRIM_FROM_BL) {
                                byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                                listener.onCrystalTrimUpdate(value[0]);
                            }
                            else if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_IDENTITY_ROOT_FROM_BL) {
                                byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                                String encRoot = "";
                                for (byte bb : value) {
                                    encRoot += String.format("%02x", bb);
                                }
                                listener.onIdentityRootUpdate(encRoot);
                            }
                            else if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_ENCRPTION_APP_FROM_BL) {
                                byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                                String encRoot = "";
                                for (byte bb : value) {
                                    encRoot += String.format("%02x", bb);
                                }
                                listener.onEncryptionRootUpdate(encRoot);
                            }
                            else {
                                State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_CHALLENGE);
                                listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_CHALLENGE);
                                byte[] challenge = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                                if (challenge != null && OtaManagerweakRef.get() != null) {
                                    // Check the data length to make sure the challenge value is right.
                                    if(challenge.length > OtaUpdateManager.mSharedSecretKeySdk.length){
                                        if (listener != null)
                                            listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_CHALLENGE_RESPONSE);
                                        return;
                                    }
                                    byte[] response = OtaManagerweakRef.get().encryptData(challenge);
                                    if(response != null)
                                        OtaManagerweakRef.get().writeDataTransferCharacteristic(response);
                                }
                            }
                            break;
                        case OtaUpdateManager.REQUEST_READ_TRANSFER_CONTROL_STATUS:
                            byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                            listener.onOtaTransferControlStatus(value[0]);
                            break;
                        case OtaUpdateManager.REQUEST_GET_APPLCATION_ONCHIP:
                            if(State.getOtaState() == State.OtaState.STATE_OTA_REFRESH_ATTRIBUTES ) {
                                final OtaUpdateManager manager = OtaManagerweakRef.get();
                                new Handler().postDelayed(new Runnable(){
                                    public void run(){
                                        if(manager != null)
                                             manager.disconnect();
                                    }
                                },10000);
                            }
                            break;
                        default:
                            break;
                        }

                    // These CS keys from Application service with Boot-loader version 4
                    if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_BT_ADDRESS) {
                        byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                        String address = "";
                        for (byte bb : value) {
                            address += String.format("%02X:", bb);
                        }
                        String ret = address.substring(0, address.length() - 1);
                        listener.onBtAddressUpdate(ret);
                    }
                    else if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_XTAL_TRIM) {
                        byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                        listener.onCrystalTrimUpdate(value[0]);
                    }
                    else if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_IDENTITY_ROOT) {
                        byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                        String encRoot = "";
                        for (byte bb : value) {
                            encRoot += String.format("%02x", bb);
                        }
                        listener.onIdentityRootUpdate(encRoot);
                    }
                    else if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_ENCRYPTION_ROOT) {
                        byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                        String encRoot = "";
                        for (byte bb : value) {
                            encRoot += String.format("%02x", bb);
                        }
                        listener.onEncryptionRootUpdate(encRoot);
                    }
                    else if (State.getOtaState() == State.OtaState.STATE_OTA_INIT_READ_CS_BLOCK ||
                             State.getOtaState() == State.OtaState.STATE_OTA_READ_CS_BLOCK) {
                        csblockNotificationValue = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                        if(csblockNotificationValue == null || csblockNotificationValue.length >16) return;
                        {
                            listener.onCsBlockData(csblockNotificationValue);
                        }
                    } else if (State.getOtaState() == State.OtaState.STATE_OTA_SET_TRANSFER_CTRL) {
                        Log.d("OtaMessagehandler","Set Boot loader transfer control in progress");
                    } else {
                        byte[] value = msg.getData().getByteArray(BtSmartService.EXTRA_VALUE);
                        if(value.length > 2){
                            Log.d("OtaMessageHandler", "onNotify was ignored,state :="+State.getOtaState().valueOf()+ ",value[0]="+value[0]+
                                    ",value[1]="+value[1]);
                        }
                    }
                }
            }
                break;
            case BtSmartService.MESSAGE_DESCRIPTOR_READ:
                break;
            case BtSmartService.MESSAGE_CHARACTERISTIC_WRITE:
                break;
            case BtSmartService.MESSAGE_DESCRIPTOR_WRITE:
                break;
            case BtSmartService.MESSAGE_DESCRIPTOR_VALUE:
                break;
            case BtSmartService.MESSAGE_WRITE_COMPLETE: {
                switch (requestId) {
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_BT:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_BT_ADDRESS);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_BT_ADDRESS);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_BT_FROM_BL:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_BT_ADDRES_FROM_BL);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_BT_ADDRES_FROM_BL);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_XLST:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_XTAL_TRIM);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_XTAL_TRIM);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_XLST_FROM_BL:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_XTAL_TRIM_FROM_BL);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_XTAL_TRIM_FROM_BL);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_IDROOT:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_IDENTITY_ROOT);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_IDENTITY_ROOT);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_IDROOT_FROM_BL:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_IDENTITY_ROOT_FROM_BL);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_IDENTITY_ROOT_FROM_BL);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_ENCROOT:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_ENCRYPTION_ROOT);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_ENCRYPTION_ROOT);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSKEYS_ENCROOT_FROM_BL:
                        State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_ENCRPTION_APP_FROM_BL);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_INIT_READ_ENCRPTION_APP_FROM_BL);
                        break;
                    case OtaUpdateManager.REQUEST_READ_CSBLOCK:
                        State.setOtaState(State.OtaState.STATE_OTA_READ_CS_BLOCK);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_READ_CS_BLOCK);
                        break;
                    case OtaUpdateManager.REQUEST_ENABLE_OTA_MODE:
                        State.setOtaState(State.OtaState.STATE_OTA_SET_MODE);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_SET_MODE);
                        break;
                    case OtaUpdateManager.REQUEST_SET_APPLCATION_ONCHIP:
                        State.setOtaState(State.OtaState.STATE_OTA_SET_CURRENT_APP);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_SET_CURRENT_APP);
                        break;
                    case OtaUpdateManager.REQUEST_SET_TRANSFER_CONTROL_READY:
                    case OtaUpdateManager.REQUEST_SET_TRANSFER_CONTROL_INPROGRESS:
                        State.setOtaState(State.OtaState.STATE_OTA_SET_TRANSFER_CTRL);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_SET_TRANSFER_CTRL);
                        break;
                    case OtaUpdateManager.REQUEST_WRITE_BYTES_TO_TARGET:
                        State.setOtaState(State.OtaState.STATE_OTA_DATA_TRANSFER);
                        if (OtaManagerweakRef.get() != null) {
                            OtaManagerweakRef.get().updateProgressbar();
                            OtaManagerweakRef.get().increment();
                            OtaManagerweakRef.get().sendNextImageChunk();
                        }
                        break;
                    case OtaUpdateManager.REQUEST_SET_TRANSFER_CONTROL_COMPLETE:
                        State.setOtaState(State.OtaState.STATE_OTA_SET_TRANSFER_COMPLETE);
                        if (OtaManagerweakRef.get() != null)
                            OtaManagerweakRef.get().updateProgressbar();
                        listener.onOtaStateUpdate(State.OtaState.STATE_OTA_SET_TRANSFER_COMPLETE);
                        break;
                    case OtaUpdateManager.REQUEST_PAUSE_DOWNLOAD:
                        if (OtaManagerweakRef.get() != null)
                            OtaManagerweakRef.get().resetSentNum();
                        State.setOtaState(State.OtaState.STATE_OTA_PAUSE_DATA_TRANSFER);
                        listener.onOtaStateUpdate(State.OtaState.STATE_OTA_PAUSE_DATA_TRANSFER);
                        break;
                    case OtaUpdateManager.REQUEST_CANCEL_DOWNLOAD:
                        if (OtaManagerweakRef.get() != null)
                            OtaManagerweakRef.get().resetSentNum();
                        State.setOtaState(State.OtaState.STATE_OTA_ABORT_DATA_TRANSFER);
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_ABORT_DATA_TRANSFER);
                        break;
                    /*This handle challenge response message*/
                    case OtaUpdateManager.REQUEST_WRITE_DATA_TRANSFER:
                        if (listener != null)
                            listener.onOtaStateUpdate(State.OtaState.STATE_OTA_WRITE_RESPONSE);
                        State.setOtaState(State.OtaState.STATE_OTA_WRITE_RESPONSE);
                        if (OtaManagerweakRef.get() != null) {
                            OtaManagerweakRef.get().registerBootloaderTransferControlNotifiocation();
                            OtaManagerweakRef.get().readBootloadVersion();

                            // This process is for V6 onwards
                            if(OtaManagerweakRef.get().getBootloaderSoftwareVersionSupported()) {
                                OtaManagerweakRef.get().readBootloaderSoftwareVersion();
                            }

                            // This process is from V5 only
                            if (OtaManagerweakRef.get().getReadCskeyFromBootloader()) {
                                OtaManagerweakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_BT_FROM_BL, OtaUpdateManager.USER_KEY_BT_ADDRESS);
                                OtaManagerweakRef.get().readDataTransferCharacteristic();
                                OtaManagerweakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_XLST_FROM_BL, OtaUpdateManager.USER_KEY_XTAL_TRIM);
                                OtaManagerweakRef.get().readDataTransferCharacteristic();
                                OtaManagerweakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_IDROOT_FROM_BL,OtaUpdateManager.USER_KEY_IDENTITY_ROOT);
                                OtaManagerweakRef.get().readDataTransferCharacteristic();
                                OtaManagerweakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_ENCROOT_FROM_BL,OtaUpdateManager.USER_KEY_ENCRYPTION_ROOT);
                                OtaManagerweakRef.get().readDataTransferCharacteristic();
                            }
                            if (listener != null)
                                listener.onOtauStarted();
                        }
                    break;
                default:
                    break;
                }
            }
                break;
            case BtSmartService.MESSAGE_INDICATION_REGISTERED:
                break;
            case BtSmartService.MESSAGE_NOTIFICATION_REGISTERED:
                break;
            case BtSmartService.MESSAGE_REQUEST_FAILED:
                switch (requestId) {
                case OtaUpdateManager.REQUEST_READ_CSKEYS_BT:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_BTADDRESS);
                    break;
                case OtaUpdateManager.REQUEST_READ_CSKEYS_XLST:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_XLST);
                    break;
                case OtaUpdateManager.REQUEST_READ_CSKEYS_IDROOT:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_IDROOT);
                    break;
                case OtaUpdateManager.REQUEST_READ_CSKEYS_ENCROOT:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_ENCROOT);
                    break;
                case OtaUpdateManager.REQUEST_READ_DATA_TRANSFER:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_CHALLENGE_RESPONSE);
                    break;
                case OtaUpdateManager.REQUEST_ENABLE_OTA_MODE:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_ENABLE_OTA);
                    break;
                case OtaUpdateManager.REQUEST_WRITE_BYTES_TO_TARGET:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_WRITE_TO_TARGET);
                    break;
                case OtaUpdateManager.REQUEST_BL_TRANSFERCONTROL_NOTIFICATION:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_BL_TRANSFERCONTROL_NOTIFICATION);
                    break;
                case OtaUpdateManager.REQUEST_SET_TRANSFER_CONTROL_INPROGRESS:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_SET_TRANSFER_CONTROL_INPROGRESS);
                    break;
                case OtaUpdateManager.REQUEST_SET_APPLCATION_ONCHIP:
                    if (listener != null)
                        listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_SET_APPLICATION_ID);
                    break;
                default:
                    if (listener != null)
                        listener.onOtaMessageUpdate(requestId);
                    break;
                }
                break;
            case BtSmartService.MESSAGE_REQUEST_NULL_CHARACTERISTIC:
                if (listener != null)
                    listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_NULL_CHARACTERISTIC);
                break;
            case BtSmartService.MESSAGE_REQUEST_NULL_SERVICE:
                if (listener != null)
                    listener.onOtaMessageUpdate(OtaUpdateManager.ERROR_MESSAGE_ID_NULL_SERVICE);
                break;
            case BtSmartService.MESSAGE_GATT_STATUS_REQUEST_NOT_SUPPORTED:
                if(requestId == OtaUpdateManager.REQUEST_SET_TRANSFER_CONTROL_INPROGRESS) {
                    State.setOtaState(State.OtaState.STATE_OTA_SET_TRANSFER_CTRL);
                    if (listener != null)
                        listener.onOtaStateUpdate(State.OtaState.STATE_OTA_SET_TRANSFER_CTRL);
                }
            break;
            default:
                break;
            }
        }

    }

}
