package com.isung.it.bbandtestapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by funny on 2015-08-27.
 */
public class DeviceActivity extends Activity implements DrawerAdapter.OnItemClickListener {
    private static final String TAG = DeviceActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private Fragment mFragment;
    private String btDeviceName;
    private String btAddress;

    private BLEService leService;
    private boolean deviceConnected = false;

    private TextView tvConnectionState;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private FrameLayout mContentFrame;
    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private String[] mPlanetTitles;

    private ArrayList<ArrayList<BluetoothGattCharacteristic>> gattCharacteristic = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private ExpandableListView gattServiceList;
    private BluetoothGattCharacteristic notifyCharacteristic;
    private BluetoothGattCharacteristic selectedCharacteristic;
    private TextView tvData;
    private Button sendMsg;
    private Button sendMsg1;
    private Button sendMsg2;

    private Button sendSaveData;
    private Button btnSendTest1;
    private Button btnSendTest2;
    private Button btnSendTest3;

    private Button btnPowerOn;
    private Button btnPowerOff;
    private Button btnApi12_1,btnApi12_2,btnApi15,btnApi16,btnApi17_1,btnApi17_2,btnApi17_3,btnApi18_1,btnApi18_2,btnApi19_1,btnApi19_2,btnApi30;

    private Button mTimerSettingBtn;

    private TextView tvRcvDataCount;

    private AlertDialog mCreateUpdateCompltDialog;

    byte[] value;
    int dataCount;
    int totalDataCount;


    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            leService = ((BLEService.LocalBinder) service).getService();
            if (!leService.initialize()) {
                Log.d("mymy", "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up
            // initialization.
            leService.connect(btAddress);

            Log.d("mymy", "Service Connected");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            leService = null;

            Log.d("mymy", "Service Disconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.device_layout);

        registerReceiver(gattUpdateReceiver, makeGattUpdateIntentFilter());

        final Intent intent = getIntent();
        btDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        btAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        mPlanetTitles = getResources().getStringArray(R.array.planets_array);
        mContentFrame = (FrameLayout) findViewById(R.id.content_frame);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (RecyclerView) findViewById(R.id.left_drawer);

        ((TextView)findViewById(R.id.device_address)).setText(btAddress);
        tvConnectionState = (TextView)findViewById(R.id.connection_state);
        tvData = (TextView) findViewById(R.id.data_value);

        tvRcvDataCount = (TextView) findViewById(R.id.rcvDataCount);

        gattServiceList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        gattServiceList.setOnChildClickListener(servicesListClickListner);

        initActionBar();

        Intent gattServiceIntent = new Intent(this, BLEService.class);
        bindService(gattServiceIntent, serviceConnection, BIND_AUTO_CREATE);

        btnPowerOn = (Button)findViewById(R.id.btnPowerOn);
        btnPowerOn.setOnClickListener(btnSendMsgClick);

        btnPowerOff = (Button)findViewById(R.id.btnPowerOff);
        btnPowerOff.setOnClickListener(btnSendMsgClick);

        btnApi12_1 = (Button)findViewById(R.id.btnApi12_1);
        btnApi12_1.setOnClickListener(btnSendMsgClick);

        btnApi12_2 = (Button)findViewById(R.id.btnApi12_2);
        btnApi12_2.setOnClickListener(btnSendMsgClick);


        btnApi15 = (Button)findViewById(R.id.btnApi15);
        btnApi15.setOnClickListener(btnSendMsgClick);

        btnApi16 = (Button)findViewById(R.id.btnApi16);
        btnApi16.setOnClickListener(btnSendMsgClick);

        btnApi17_1 = (Button)findViewById(R.id.btnApi17_1);
        btnApi17_1.setOnClickListener(btnSendMsgClick);

        btnApi17_2 = (Button)findViewById(R.id.btnApi17_2);
        btnApi17_2.setOnClickListener(btnSendMsgClick);

        btnApi17_3 = (Button)findViewById(R.id.btnApi17_3);
        btnApi17_3.setOnClickListener(btnSendMsgClick);

        btnApi18_1 = (Button)findViewById(R.id.btnApi18_1);
        btnApi18_1.setOnClickListener(btnSendMsgClick);

        btnApi18_2 = (Button)findViewById(R.id.btnApi18_2);
        btnApi18_2.setOnClickListener(btnSendMsgClick);

        btnApi19_1 = (Button)findViewById(R.id.btnApi19_1);
        btnApi19_1.setOnClickListener(btnSendMsgClick);

        btnApi19_2 = (Button)findViewById(R.id.btnApi19_2);
        btnApi19_2.setOnClickListener(btnSendMsgClick);

        btnApi30 = (Button)findViewById(R.id.btnApi30);
        btnApi30.setOnClickListener(btnSendMsgClick);

        sendMsg = (Button)findViewById(R.id.btnSendData);
        sendMsg.setOnClickListener(btnSendMsgClick);

        sendMsg1 = (Button)findViewById(R.id.btnSendData1);
        sendMsg1.setOnClickListener( btnSendMsgClick );

        sendMsg2 = (Button)findViewById(R.id.btnSendData2);
        sendMsg2.setOnClickListener( btnSendMsgClick );

        sendSaveData = (Button)findViewById(R.id.btnExtSave);
        sendSaveData.setOnClickListener( btnSendMsgClick );

        btnSendTest1 = (Button)findViewById(R.id.btnSendTest1);
        btnSendTest1.setOnClickListener( btnSendMsgClick );

        btnSendTest2 = (Button)findViewById(R.id.btnSendTest2);
        btnSendTest2.setOnClickListener( btnSendMsgClick );

        btnSendTest3 = (Button)findViewById(R.id.btnSendTest3);
        btnSendTest3.setOnClickListener( btnSendMsgClick );

        mTimerSettingBtn = (Button)findViewById(R.id.timer_setting_btn);
        mTimerSettingBtn.setOnClickListener( btnSendMsgClick );

        LinearLayoutManager mLinearLayoutmanager = new LinearLayoutManager(this);
        mDrawerList.setLayoutManager(mLinearLayoutmanager);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // improve performance by indicating the list if fixed size.
        mDrawerList.setHasFixedSize(true);

        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new DrawerAdapter(mPlanetTitles, this));

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        value = new byte[10];

        if (mContentFrame != null) {
            mContentFrame.setVisibility(View.GONE);
        }

    }

    private void initActionBar() {
        //getActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle(R.string.app_name);
        getActionBar().setBackgroundDrawable( getResources().getDrawable(R.color.action_bar_color) );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // handle click event on home icon(drawer icon)
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* The click listener for RecyclerView in the navigation drawer */
    @Override
    public void onClick(View view, int position) {
        selectItem(position);
    }

    private void selectItem(int position) {

        if(mFragment instanceof OTAFragment && ((OTAFragment)mFragment).isOTAUpdate()){
            Log.d("JSLEE","instanceof OTAFragment");
            Toast.makeText(DeviceActivity.this, "OTA 진행 중에는 사용할 수 없습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (position) {
            case 0: //OTA
            {
                mFragment = OTAFragment.newInstance(position);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content_frame, mFragment);
                ft.commit();

                // update the main content by replacing fragments
                if (mContentFrame != null) {
                    mContentFrame.setVisibility(View.VISIBLE);
                }
                break;
            }
//            case 0: //ON/OFF 시간예약
//            {
//                moveTimerSetting();
//
//                // update the main content by replacing fragments
//                if (mContentFrame != null) {
//                    mContentFrame.setVisibility(View.VISIBLE);
//                }
//                break;
//            }
//            case 1: //초기화
//            {
//                //TODO 인트로이동
//                AlertDialog dialog = createResetDialog();
//                dialog.show();
//                break;
//            }
//            case 2: //정보
//            {
//                //TODO 다이얼로그
//                AlertDialog dialog = createVersionDialog();
//                dialog.show();
//
////                fragment = InfoFragment.newInstance(position);
////                FragmentManager fragmentManager = getFragmentManager();
////                FragmentTransaction ft = fragmentManager.beginTransaction();
////                ft.replace(R.id.content_frame, fragment);
////                ft.commit();
//                break;
//            }
//            case 3: //OTA
//            {
//                mFragment = OTAFragment.newInstance(position);
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction ft = fragmentManager.beginTransaction();
//                ft.replace(R.id.content_frame, mFragment);
//                ft.commit();
//
//                // update the main content by replacing fragments
//                if (mContentFrame != null) {
//                    mContentFrame.setVisibility(View.VISIBLE);
//                }
//                break;
//            }
            default: {
                mFragment = TimerSettingFragment.newInstance();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content_frame, mFragment);
                ft.commit();

                // update the main content by replacing fragments
                if (mContentFrame != null) {
                    mContentFrame.setVisibility(View.VISIBLE);
                }
                break;
            }
        }

        // update selected item title, then close the drawer
        setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    public void contentFrameGone(){
        if (mContentFrame != null && mContentFrame.isShown()) {
            mContentFrame.setVisibility(View.GONE);

            getActionBar().setTitle(R.string.app_name);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Log.d("JSLEE","onBackPressed");

        timeBtnUiInit();

        if(mFragment instanceof OTAFragment && ((OTAFragment)mFragment).isOTAUpdate()){
            Log.d("JSLEE","instanceof OTAFragment");
            AlertDialog dialog = createOtaCancelDialog();
            dialog.show();
            return;
        }

        if (mContentFrame != null && mContentFrame.isShown()) {
            mContentFrame.setVisibility(View.GONE);

            getActionBar().setTitle(R.string.app_name);
        } else {
            super.onBackPressed();
        }

//        FragmentManager fm = getFragmentManager();
//        if (fm.getBackStackEntryCount() >= 0) {
//            FragmentTransaction ft = fm.beginTransaction();
//
//            ft.hide(fragment);
//            ft.commit();
//        } else {
//            super.onBackPressed();
//        }

        //clearStack();
    }

    private void moveTimerSetting(){
        //현재 시간 전송
        if(mHandler != null){
            mHandler.postDelayed(mCurrentSetTime, 500);
        }

        if(deviceConnected == true){
            if (mContentFrame != null) {
                mContentFrame.setVisibility(View.VISIBLE);
            }
            mFragment = TimerSettingFragment.newInstance();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.content_frame, mFragment);
            ft.commit();
        } else {
            Toast.makeText(DeviceActivity.this, "밴드와 연결후 재시도 하세요.",Toast.LENGTH_LONG).show();
        }
    }

    private void clearStack() {
        final FragmentManager fm = getFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    View.OnClickListener btnSendMsgClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch( v.getId() ) {
                case R.id.timer_setting_btn :
                    moveTimerSetting();
                    break;
                case R.id.btnPowerOn :

                    //TODO jslee API 연동

//                    mHandler.postDelayed(mMyTask, 700);
//
//                    btnPowerOn.setEnabled(false);
//                    btnPowerOff.setEnabled(false);
//
//                    sendBLELightOnOff(true);

                    break;
                case R.id.btnPowerOff :

                    //TODO jslee API 연동

//                    mHandler.postDelayed(mMyTask, 700);
//
//                    btnPowerOn.setEnabled(false);
//                    btnPowerOff.setEnabled(false);
//
//                    sendBLELightOnOff(false);

                    break;
                case R.id.btnApi12_1 : //모드 변경 default
                    value = BLEApiUtils.BLE_CRC_DATA(0x12, 0x00);
                    bleDataSend(value);
                    break;
                case R.id.btnApi12_2 : //모드 변경 카메라 모드
                    value = BLEApiUtils.BLE_CRC_DATA(0x12, 0x01);
                    bleDataSend(value);
                    break;
                case R.id.btnApi15 : //배터리
                    value = BLEApiUtils.BLE_CRC_DATA(0x15);
                    bleDataSend(value);
                    break;
                case R.id.btnApi16 : //ota모드
                    value = BLEApiUtils.BLE_CRC_DATA(0x16);
                    bleDataSend(value);
                    break;
                case R.id.btnApi17_1 : //전화 수신
                    sendBLECallNoti(BLEApiUtils.BLE_API_CALL_NOTI_PARAM_CALL_RECEIVING);
                    break;
                case R.id.btnApi17_2 : //전화 수신 완료
                    sendBLECallNoti(BLEApiUtils.BLE_API_CALL_NOTI_PARAM_CALL_RECEVED);
                    break;
                case R.id.btnApi17_3 : //부재중 알림
                    sendBLECallNoti(BLEApiUtils.BLE_API_CALL_NOTI_PARAM_UNANSWERED_CALL);
                    break;
                case R.id.btnApi18_1 : //문자 수신
                    value = BLEApiUtils.BLE_CRC_DATA(0x18,0x01);
                    bleDataSend(value);
                    break;
                case R.id.btnApi18_2 : //문자 알림 삭제
                    value = BLEApiUtils.BLE_CRC_DATA(0x18,0x02);
                    bleDataSend(value);
                    break;
                case R.id.btnApi19_1 : //app 알림
                    value = BLEApiUtils.BLE_CRC_DATA(0x19,0x01,0x00,0xFF,0x00,0x14);
                    bleDataSend(value);
                    break;
                case R.id.btnApi19_2 : //app 알림 삭제
                    value = BLEApiUtils.BLE_CRC_DATA(0x19,0x02,0x00,0x00,0x00,0x00);
                    bleDataSend(value);
                    break;
                case R.id.btnApi30 : //버젼 가져오기
                    value = BLEApiUtils.BLE_CRC_DATA(0x30);
                    bleDataSend(value);
                    break;

                case R.id.btnSendData2 : // Blue
                    /*
                    // for naver
                    value[0] = (byte)0xFF;
                    value[1] = 0x12;
                    value[2] = 0x01;
                    value[3] = 0x00;
                    */

                    // b-band test set color 0
                    value[0] = (byte)0xFF;
                    value[1] = 0x13;
                    value[2] = 0x01;
                    value[3] = 0x78;

                    selectedCharacteristic.setValue(value);
                    leService.writeCharacteristic(selectedCharacteristic);
                    break;
                case R.id.btnExtSave : // 센서 저장모드
                    /*
                    // for naver
                    value[0] = (byte)0xFF;
                    value[1] = 0x12;
                    value[2] = 0x01;
                    value[3] = 0x03;
*/

                    // test of bband - led on
                    value[0] = (byte)0xFF;
                    value[1] = 0x12;
                    value[2] = 0x01;
                    value[3] = 0x01;

                    selectedCharacteristic.setValue(value);
                    leService.writeCharacteristic(selectedCharacteristic);
                    break;
                case R.id.btnSendTest1 : // Data Sync (테스트용)
                    /*
                    // for naver
                    value[0] = (byte)0xFF;
                    value[1] = (byte)0xC8;
                    value[2] = 0x0;
                    value[3] = 0x03;
                    */

                    // test of bband - led off
                    value[0] = (byte)0xFF;
                    value[1] = 0x12;
                    value[2] = 0x01;
                    value[3] = 0x00;

                    selectedCharacteristic.setValue(value);
                    leService.writeCharacteristic(selectedCharacteristic);
                    break;
                case R.id.btnSendTest2 : // 저장된 데이터 수
                    /*
                    // for naver
                    value[0] = (byte)0xFF;
                    value[1] = (byte)0xCB;
                    value[2] = 0x0;
                    value[3] = 0x03;
                    */

                    // b-band test set color 0
                    value[0] = (byte)0xFF;
                    value[1] = 0x13;
                    value[2] = 0x01;
                    value[3] = (byte)0xB4;

                    selectedCharacteristic.setValue(value);
                    leService.writeCharacteristic(selectedCharacteristic);
                    break;
                case R.id.btnSendTest3 : // 데이터 클리어
                    /*
                    // for naver
                    value[0] = (byte)0xFF;
                    value[1] = 0x17;
                    value[2] = 0x0;
                    value[3] = 0x03;
                    */

                    // b-band test set color 0
                    value[0] = (byte)0xFF;
                    value[1] = 0x13;
                    value[2] = 0x01;
                    value[3] = (byte)0xFF;

                    selectedCharacteristic.setValue(value);
                    leService.writeCharacteristic(selectedCharacteristic);
                    break;
            }
        }
    };

    private Handler mHandler = new Handler();

    private Runnable mMyTask = new Runnable() {
        @Override
        public void run() {
            Log.d("mymy", "run run");

            btnPowerOn.setEnabled(true);
            btnPowerOff.setEnabled(true);
        }
    };

    private Runnable mCurrentSetTime = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "mCurrentSetTime()");

            sendBLECurrentTime();
        }
    };


    private Runnable mTimeReservationConfirm = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "mTimeReservationConfirm()");

            sendBLETimeReservationConfirm();
        }
    };

    private final ExpandableListView.OnChildClickListener servicesListClickListner = new ExpandableListView.OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

            Log.d(TAG, "servicesListClickListner onChildClick gattCharacteristic=" + gattCharacteristic);

            if (gattCharacteristic != null) {
                final BluetoothGattCharacteristic characteristic = gattCharacteristic.get(groupPosition).get(childPosition);
                selectedCharacteristic = characteristic;
                final int charaProp = characteristic.getProperties();

                ((MainActivity)MainActivity.ct).selectedCharacteristic = characteristic;

                if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                    if (notifyCharacteristic != null) {
                        leService.setCharacteristicNotification(notifyCharacteristic, false);
                        notifyCharacteristic = null;
                    }

                    leService.readCharacteristic(characteristic);

                    /*
                    byte[] value = new byte[1];
                    value[0] = (byte) (21 & 0xFF);
                    characteristic.setValue(value);
                    */

                    // characteristic.setValue("test");

                    /*
                    byte[] value = new byte[2];
                    value[0] = 0x01;
                    value[1] = 0x03;

                    characteristic.setValue(value);

                    leService.writeCharacteristic(characteristic);
                    */
                }
                if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                    notifyCharacteristic = characteristic;
                    leService.setCharacteristicNotification(characteristic, true);
                }
                return true;
            }
            return false;
        }
    };

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BLEService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BLEService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    @Override
    protected void onResume() {
        super.onResume();

        totalDataCount = 0;

        if(leService != null) {
            final boolean result = leService.connect(btAddress);
            Log.d("mymy", "Connect request result=" + result);
        }

        timeBtnUiInit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "unregisterReceiver(gattUpdateReceiver)");
        unregisterReceiver(gattUpdateReceiver);
        unbindService(serviceConnection);
        leService.disconnect();
        leService = null;
    }

    public void bleDisconnect(){
        if(leService != null){
            leService.disconnect();
        }
    }

    public boolean isDeviceConnected(){
        return deviceConnected;
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectionState.setText(resourceId);
            }
        });
    }

    private final BroadcastReceiver gattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (leService.ACTION_GATT_CONNECTED.equals(action)) {
                Log.d("mymy", "gattUpdateReceiver, GATT_CONNECTED");
                deviceConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();

//                //현재 시간 전송
//                if(mHandler != null){
//                    mHandler.postDelayed(mCurrentSetTime, 500);
//                }
//
//                //on off 예약시간 및 사용여부 값 가져오기
//                if(mHandler != null){
//                    mHandler.postDelayed(mTimeReservationConfirm, 1000);
//                }
//
//                //현재 시간 전송 2차
//                if(mHandler != null){
//                    mHandler.postDelayed(mCurrentSetTime, 1500);
//                }

            } else if (leService.ACTION_GATT_DISCONNECTED.equals(action)) {
                Log.d("mymy", "gattUpdateReceiver, GATT_DISCONNECTED");
                Log.d("JSLEE", "gattUpdateReceiver, GATT_DISCONNECTED");
                deviceConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (leService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the
                // user interface.
                Log.d("mymy", "gattUpdateReceiver, GATT_SERVICE_DISCOVERED");
                displayGattServices(leService.getSupportedGattServices());
            } else if (leService.ACTION_DATA_AVAILABLE.equals(action)) {
                Log.d("mymy", "gattUpdateReceiver, DATA_AVAILABLE mFragment = " + mFragment);

                Log.d(TAG, "BLEReadMessage() send");
                byte[] message = intent.getByteArrayExtra(BLEService.ACTION_RECEVE_DATA);
                Log.d(TAG, "BLEReadMessage() data.length = " + message.length);

                StringBuilder sb = new StringBuilder(message.length * 2);
                for( byte b : message) {
                    sb.append(String.format("%02x", b & 0xff)).append(" ");
                }
                Log.d(TAG, "BLEReadMessage data : " + sb);

                Toast.makeText(DeviceActivity.this, "BLEReadMessage = " + sb.toString() ,Toast.LENGTH_SHORT).show();

                //TODO CRC 체크 필요

                if(message != null && message.length >= 2) {
                    switch (message[1]) {

                        //예약시간 및 사용여부 값이 들어오면 프리페어런스에 값을 저장한다.
                        //버튼 UI도 갱신
                        case (byte) BLEApiUtils.BLE_API_TIME_RESERVATION_CONFIRM: {
                            Log.d(TAG, "BLE_API_TIME_RESERVATION_CONFIRM");

                            SharedPreferences activityPrefs = BLEApiUtils.getPreferences(DeviceActivity.this);

                            if(message.length >= 7){
                                int mOnHour = message[3];
                                int mOnMinute = message[4];
                                int mOffHour = message[5];
                                int mOffMinute = message[6];

                                SharedPreferences.Editor editor = activityPrefs.edit();
                                editor.putInt(Preferences.SETTING_ON_TIME_HOUR, mOnHour);
                                editor.putInt(Preferences.SETTING_ON_TIME_MIN, mOnMinute);
                                editor.putInt(Preferences.SETTING_OFF_TIME_HOUR, mOffHour);
                                editor.putInt(Preferences.SETTING_OFF_TIME_MIN, mOffMinute);
                                editor.commit();
                            }

                            if(message.length >= 8){
                                Log.d(TAG,"SETTING_IS_TIME_RESERVATION_USE");
                                int mIsUseCheck = message[7];

                                SharedPreferences.Editor editor = activityPrefs.edit();
                                editor.putInt(Preferences.SETTING_IS_TIME_RESERVATION_USE, mIsUseCheck);
                                editor.commit();

                                //버튼UI 갱신
                                timeBtnUiInit();
                            }

                            break;
                        }
                    }
                }

                if(mFragment != null && mFragment instanceof TimerSettingFragment){
                    ((TimerSettingFragment)mFragment).BLEReadMessage(message);
                }

//                displayData(intent.getStringExtra(leService.EXTRA_DATA));
            }
        }
    };

    private void timeBtnUiInit(){

        SharedPreferences activityPrefs = BLEApiUtils.getPreferences(this);
        int mIsUseCheck = activityPrefs.getInt(Preferences.SETTING_IS_TIME_RESERVATION_USE, 0);

        //버튼UI 갱신
        if(mIsUseCheck == 1){
            mTimerSettingBtn.setText("예약 ON");
        } else {
            mTimerSettingBtn.setText("예약 OFF");
        }
    }

    private void displayData(String data) {
        if (data != null) {
            dataCount = data.length();
            tvData.setText(data);

            totalDataCount = dataCount + totalDataCount;

            Log.d("mymy", "displayData : " + data);

            byte[] value = data.getBytes();

            /*
            funnylogic
            Server로부터 받은 데이터를 다시 Server로 보낸다.

            byte[] value = data.getBytes();
            selectedCharacteristic.setValue(value);
            leService.writeCharacteristic(selectedCharacteristic);
            */

            tvRcvDataCount.setText( Integer.toString( totalDataCount ) );
        }
    }

    private void clearUI() {
        gattServiceList.setAdapter((SimpleExpandableListAdapter) null);
        tvData.setText(R.string.no_data);
    }

    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null)
            return;
        String uuid = null;
        String unknownServiceString = getResources().getString(
                R.string.unknown_service);
        String unknownCharaString = getResources().getString(
                R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData = new ArrayList<ArrayList<HashMap<String, String>>>();
        gattCharacteristic = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(LIST_NAME, GattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            Log.d("mymy", "Gatt service : " + uuid);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(LIST_NAME, GattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);

                Log.d("mymy", "Gatt character : " + uuid);

                gattCharacteristicGroupData.add(currentCharaData);

                if( uuid.equals("000092a4-0000-1000-8000-00805f9b34fb")) { //0000870c-0000-1000-8000-00805f9b34fb  //e18c92a4-1ad0-40b1-abcc-d955d0a5998b
                    Log.d("mymy", "find characteristic");
                    selectedCharacteristic = gattCharacteristic;
                }
            }
            gattCharacteristic.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);

            Log.d(TAG, "selectedCharacteristic = " + selectedCharacteristic);
            if(selectedCharacteristic != null){
                leService.setCharacteristicNotification(selectedCharacteristic, true);
            }
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this, gattServiceData,
                android.R.layout.simple_expandable_list_item_2, new String[] {
                LIST_NAME, LIST_UUID }, new int[] { android.R.id.text1,
                android.R.id.text2 }, gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2, new String[] {
                LIST_NAME, LIST_UUID }, new int[] { android.R.id.text1,
                android.R.id.text2 });
        gattServiceList.setAdapter(gattServiceAdapter);
    }

    private AlertDialog createVersionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("버젼정보");
        builder.setMessage("Ver : 1.0.0");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        AlertDialog dialog = builder.create();
        return dialog;
    }

    private AlertDialog createResetDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Reset");
        builder.setMessage("초기화 합니다 실행하시겠습니까?");

        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
                startActivity(new Intent(DeviceActivity.this, SplashActivity.class));
            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        AlertDialog dialog = builder.create();
        return dialog;
    }

    private AlertDialog createOtaCancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Warning");
        builder.setMessage("업데이트를 강제 종료 합니다.\n정말 진행하시겠습니까?");

        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
                startActivity(new Intent(DeviceActivity.this, SplashActivity.class));
            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        AlertDialog dialog = builder.create();
        return dialog;
    }


    private AlertDialog createUpdateCompltDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Notice");
        builder.setMessage("업데이트가 끝났습니다.\n장치를 재 검색 합니다.");
        builder.setCancelable(false);

        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                finish();
                startActivity(new Intent(DeviceActivity.this, SplashActivity.class));
            }
        });

        AlertDialog dialog = builder.create();
        return dialog;
    }

    public void showUpdateComplitDialog(){
        if(mCreateUpdateCompltDialog == null){
            mCreateUpdateCompltDialog = createUpdateCompltDialog();
        }
        if(mCreateUpdateCompltDialog.isShowing() == false){
            mCreateUpdateCompltDialog.show();
        }
    }

    /**
     * 4.4 현재 시간 동기화
     */
    public void sendBLECurrentTime(){
        Log.d(TAG,"sendBLECurrentTime()");
        CurrentTime currentTime = CurrentTime.getInstance();
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_CURRENT_TIME, currentTime.getHour() , currentTime.getMinute(), currentTime.getSecond());
        bleDataSend(value);
    }

    /**
     * 4.5 on 시간 장치에 저장
    */
    public void sendBLEOnTimeSetting(int hour, int min){
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_ON_TIME_SETTING, hour, min);
        bleDataSend(value);
    }

    /**
     * 4.6 off 시간 장치에 저장
     */
    public void sendBLEOffTimeSetting(int hour, int min){
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_OFF_TIME_SETTING, hour, min);
        bleDataSend(value);
    }

    /**
     * 4.9 전원 제어 (on / off)
     */
    private void sendBLELightOnOff(boolean isUse){
        int useValue = 0;
        if(isUse == true){
            useValue = 0x01;
        } else {
            useValue = 0x00;
        }
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_LIGHT_ON_OFF, useValue);
        bleDataSend(value);

        SystemClock.sleep(100);

        bleDataSend(value);
    }

    /**
     * 4.10 자동 on/off 기능 설정
     */
    public void sendBLEAutoOnOffSetting(boolean isUse){
        int useValue = 0;
        if(isUse == true){
            useValue = 0x01;
        } else {
            useValue = 0x00;
        }
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_AUTO_ON_OFF_SETTING, useValue);
        bleDataSend(value);
    }

    /**
     * 4.11 자동 on / off 시간 요청
     * 4.12통합 api 사용 예정
     */
    public void sendBLEAutoTimeRequest(){
        Log.d(TAG, "sendBLEAutoTimeRequest()");
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_AUTO_ON_OFF_TIME_REQUEST, 0x00);
        for(int s: value){
            Log.d(TAG,"sendBLEAutoTimeRequest = " + s);
        }
        bleDataSend(value);
    }

    /**
     * 4.12 자동 On/Off 시간 및 사용여부 요청
     */
    public void sendBLETimeReservationConfirm(){
        Log.d(TAG, "sendBLETimeReservationConfirm()");
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_TIME_RESERVATION_CONFIRM);
        bleDataSend(value);
    }


    /**
     * Bband
     * 0x17 전화 알림
     *
     * BLE_API_CALL_NOTI_PARAM_CALL_RECEIVING = 0x01;
     * BLE_API_CALL_NOTI_PARAM_CALL_RECEVED = 0x02;
     * BLE_API_CALL_NOTI_PARAM_UNANSWERED_CALL = 0x03;
     */
    public void sendBLECallNoti(int param){
        Log.d(TAG, "sendBLETimeReservationConfirm()");
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_CALL_NOTI, (byte)param);
        bleDataSend(value);
    }

    /**
     * Bband bmc200공통
     * 4.13 펌웨어 버젼 요청
     */
    public void sendBLEGetVersion(){
        Log.d(TAG, "sendBLEGetVersion()");
        value = BLEApiUtils.BLE_CRC_DATA(BLEApiUtils.BLE_API_GET_VERSION);
        bleDataSend(value);
    }

    private void bleDataSend(byte[] value){
        if( selectedCharacteristic != null ) {
            selectedCharacteristic.setValue(value);
            if(leService != null){
                leService.writeCharacteristic(selectedCharacteristic);
            }
        }
        else {
            Log.d("mymy", "bleDataSend selectedCharacteristic is null");
        }
    }

}

