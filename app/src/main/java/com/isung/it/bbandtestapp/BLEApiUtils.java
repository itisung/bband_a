package com.isung.it.bbandtestapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

public class BLEApiUtils {
    private static final String TAG = BLEApiUtils.class.getSimpleName();

    public static final int BLE_API_CURRENT_TIME = 0x0A;
    public static final int BLE_API_ON_TIME_SETTING = 0x0B;
    public static final int BLE_API_OFF_TIME_SETTING = 0x0C;
    public static final int BLE_API_LIGHT_ON_OFF = 0x13;
    public static final int BLE_API_AUTO_ON_OFF_SETTING = 0x0F;
    public static final int BLE_API_AUTO_ON_OFF_TIME_REQUEST = 0x14;
    public static final int BLE_API_TIME_RESERVATION_CONFIRM = 0x15;


    //BBand
    public static final int BLE_API_CALL_NOTI = 0x17;
    public static final int BLE_API_CALL_NOTI_PARAM_CALL_RECEIVING = 0x01;
    public static final int BLE_API_CALL_NOTI_PARAM_CALL_RECEVED = 0x02;
    public static final int BLE_API_CALL_NOTI_PARAM_UNANSWERED_CALL = 0x03;

    public static final int BLE_API_GET_VERSION = 0x30;



    public static SharedPreferences getPreferences(Context context){
        SharedPreferences prefs = context.getSharedPreferences("Setting", Activity.MODE_PRIVATE);
        return prefs;
    }

    /**
     * 1자리 숫자일 경우 앞에 0을 붙여준다
     * @author nowleejs
     * 2012. 9. 7.
     * @param c
     * @return
     */
    public static  String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    public static byte[] BLE_CRC_DATA(int packetType) {
        byte[] value = new byte[4];
        int packetStart = 0xFF;
        int packetSize = 0x00;
        Log.d(TAG,"BLE_CRC_DATA :: packetStart = " + packetStart + " packetType= " + packetType);
        int sum = packetStart + packetType + packetSize;

        value[0] = (byte)packetStart;
        value[1] = (byte)packetType;
        value[2] = (byte)packetSize;
        value[3] = (byte)CRC(sum);

        return value;
    }
    public static byte[] BLE_CRC_DATA(int packetType,int packetData1) {
        byte[] value = new byte[5];
        int packetStart = 0xFF;
        int packetSize = 0x01;
        Log.d(TAG,"BLE_CRC_DATA :: packetStart = " + packetStart + " packetType= " + packetType + " packetData1= " + packetData1);
        int sum = packetStart + packetType + packetSize + packetData1;

        value[0] = (byte)packetStart;
        value[1] = (byte)packetType;
        value[2] = (byte)packetSize;
        value[3] = (byte)packetData1;
        value[4] = (byte)CRC(sum);

        return value;
    }
    public static byte[] BLE_CRC_DATA(int packetType,int packetData1,int packetData2) {
        byte[] value = new byte[6];
        int packetStart = 0xFF;
        int packetSize = 0x02;
        Log.d(TAG,"BLE_CRC_DATA :: packetStart = " + packetStart + " packetType= " + packetType + " packetData1= " + packetData1 + " packetData2= " + packetData2);
        int sum = packetStart + packetType + packetSize + packetData1 + packetData2;

        value[0] = (byte)packetStart;
        value[1] = (byte)packetType;
        value[2] = (byte)packetSize;
        value[3] = (byte)packetData1;
        value[4] = (byte)packetData2;
        value[5] = (byte)CRC(sum);

        return value;
    }
    public static byte[] BLE_CRC_DATA(int packetType,int packetData1,int packetData2,int packetData3) {
        byte[] value = new byte[7];
        int packetStart = 0xFF;
        int packetSize = 0x03;
        Log.d(TAG,"BLE_CRC_DATA :: packetStart = " + packetStart + " packetType= " + packetType + " packetData1= " + packetData1 + " packetData2= " + packetData2 + " packetData3= " + packetData3);
        int sum = packetStart + packetType + packetSize + packetData1 + packetData2 + packetData3;

        value[0] = (byte)packetStart;
        value[1] = (byte)packetType;
        value[2] = (byte)packetSize;
        value[3] = (byte)packetData1;
        value[4] = (byte)packetData2;
        value[5] = (byte)packetData3;
        value[6] = (byte)CRC(sum);

        return value;
    }
    public static byte[] BLE_CRC_DATA(int packetType,int packetData1,int packetData2,int packetData3,int packetData4) {
        byte[] value = new byte[8];
        int packetStart = 0xFF;
        int packetSize = 0x04;
        Log.d(TAG,"BLE_CRC_DATA :: packetStart = " + packetStart + " packetType= " + packetType + " packetData1= " + packetData1 + " packetData2= " + packetData2 + " packetData3= " + packetData3 + " packetData4= " + packetData4);
        int sum = packetStart + packetType + packetSize + packetData1 + packetData2 + packetData3 +packetData4;

        value[0] = (byte)packetStart;
        value[1] = (byte)packetType;
        value[2] = (byte)packetSize;
        value[3] = (byte)packetData1;
        value[4] = (byte)packetData2;
        value[5] = (byte)packetData3;
        value[6] = (byte)packetData4;
        value[7] = (byte)CRC(sum);

        return value;
    }

    public static byte[] BLE_CRC_DATA(int packetType,int packetData1,int packetData2,int packetData3,int packetData4,int packetData5) {
        byte[] value = new byte[9];
        int packetStart = 0xFF;
        int packetSize = 0x05;
        Log.d(TAG,"BLE_CRC_DATA :: packetStart = " + packetStart + " packetType= " + packetType + " packetData1= " + packetData1 + " packetData2= " + packetData2 + " packetData3= " + packetData3 + " packetData4= " + packetData4 + " packetData5= " + packetData5);
        int sum = packetStart + packetType + packetSize + packetData1 + packetData2 + packetData3 +packetData4 +packetData5;

        value[0] = (byte)packetStart;
        value[1] = (byte)packetType;
        value[2] = (byte)packetSize;
        value[3] = (byte)packetData1;
        value[4] = (byte)packetData2;
        value[5] = (byte)packetData3;
        value[6] = (byte)packetData4;
        value[7] = (byte)packetData5;
        value[8] = (byte)CRC(sum);

        return value;
    }

    public static int CRC(int data) {
        Log.d(TAG,"data = " + data);
        int sum = 0xFFFF - data;
        sum = sum & 0x00FF;

        String hexStr = Integer.toHexString(sum);

        if (TextUtils.isEmpty(hexStr) == false && (hexStr.length() > 2) ){
            hexStr = hexStr.substring(hexStr.length()-2);
        }
        Log.d(TAG, "hexStr ="+hexStr);

        sum = Integer.parseInt(hexStr, 16);
//        sum = 0xFF^sum; //이 값을 넣으면 다른 값이 나옴(김종두 책임님 예제 참고한 내용중)
        Log.d(TAG, "CRC ="+sum);

        return sum;
    }
}
