/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;

import com.csr.btsmart.BtSmartService;
import com.csr.btsmart.BtSmartService.BtSmartUuid;

import java.lang.ref.WeakReference;

class ConnectionHandler extends Handler {

    private final static byte UNKNOWN_SERVICE_MODE     = 0x0;
    private final static byte APPLICATION_SERVICE_MODE = 0x01;
    private final static byte BOOTLOADER_SERVICE_MODE  = 0x02;
    private final static byte REFRESH_SERVICE_MODE      = 0x03;
    
    private ConnectionHandler mHandler;

    /**
     * Initialise state machine
     * 
     */
    private ConnectionHandler initConnectionHandler(Looper looper) {
        mHandler = new ConnectionHandler(looper);
        return mHandler;
    }

    private ConnectionHandler(Looper looper) {
        super(looper);
    }
    
    /**
     * Constructor creates a StateMachine in main thread.
     * 
     * @param name
     *            The name of the state machine
     */
    protected ConnectionHandler(String name) {
        Looper looper = Looper.myLooper();
        initConnectionHandler(looper);
    }

    @Override
    public void handleMessage(Message msg) {

        IOtaMessageListener listener = null;
        WeakReference<OtaUpdateManager> weakRef = new WeakReference<OtaUpdateManager>(OtaUpdateManager.getInstance());
        if (weakRef.get() != null)
            listener = weakRef.get().getListener();
        
        switch (msg.what) {
        case BtSmartService.MESSAGE_CONNECTED:
        {
            byte mode = checkServices(msg);
            if(mode == APPLICATION_SERVICE_MODE){
                Log.d("Connectionhander", "in application mode");
                handleApplicationService();
            }
            else if (mode == BOOTLOADER_SERVICE_MODE) {
                Log.d("Connectionhander","in bootloader mode");
                handleBootloaderService();
            } else if (mode == REFRESH_SERVICE_MODE) {
                Log.d("Connectionhander","Refresh Service mode");
                break;
            } else {
                State.setState(State.ConnectionState.STATE_INVALID);
                Log.d("Connectionhander","in unknown mode");
                if (listener != null)
                    listener.onConnectionStateUpdate(-1);
            }
        }
           break;
        case BtSmartService.MESSAGE_DISCONNECTED: {
            if (weakRef.get() != null) {
                weakRef.get().disconnect();
                if (State.getOtaState() == State.OtaState.STATE_OTA_SET_MODE)
                    weakRef.get().connect();
                else if(State.getOtaState() == State.OtaState.STATE_OTA_SET_TRANSFER_COMPLETE) {
                    if(weakRef.get().getIsMeshDevice())
                        weakRef.get().connect(true);
                }
            }

            if (listener != null)
                listener.onConnectionStateUpdate(BtSmartService.MESSAGE_DISCONNECTED);
        }
            break;
        // Indicates the remote device is bonded (paired).
        case BtSmartService.MESSAGE_DEVICE_BONDED:
            if (listener != null)
                listener.onBondStateUpdate(BtSmartService.MESSAGE_DEVICE_BONDED);
            break;
        // Indicates bonding (pairing) is in progress with the remote device.
        case BtSmartService.MESSAGE_DEVICE_BONDING:
            if (listener != null)
                listener.onBondStateUpdate(BtSmartService.MESSAGE_DEVICE_BONDING);
            break;
        // Indicates the remote device is not bonded (paired).
        case BtSmartService.MESSAGE_DEVICE_BONDNONE:
            if (listener != null)
                listener.onBondStateUpdate(BtSmartService.MESSAGE_DEVICE_BONDNONE);
            break;
        default:
            break;
        }

    }

    /**
     * Handle Application service attributes
     *
     */
    private void handleApplicationService() {
        IOtaMessageListener listener = null;
        WeakReference<OtaUpdateManager> weakRef = new WeakReference<OtaUpdateManager>(OtaUpdateManager.getInstance());
        if (weakRef.get() != null)
            listener = weakRef.get().getListener();
        
        State.setState(State.ConnectionState.STATE_CONNECTED);
        State.setOtaState(State.OtaState.STATE_OTA_IDLE);

        if (weakRef.get() != null) {

            /* After refreshing service attributes.
             * It read new software version and trigger pairing and disconnected.
             */
            if(weakRef.get().getRefreshAttributesDone()) {
                weakRef.get().setRefreshAttributesDone(false);
                weakRef.get().getSoftwareVersion();
                weakRef.get().readAuthenticatedCharacteristicInApp();
                State.setOtaState(State.OtaState.STATE_OTA_REFRESH_ATTRIBUTES);
                return;
            }

            if (listener != null)
                listener.onConnectionStateUpdate(BtSmartService.MESSAGE_CONNECTED);

            weakRef.get().getSoftwareVersion();
            weakRef.get().readAuthenticatedCharacteristicInApp();
            weakRef.get().registerServiceChangedIndication();
            weakRef.get().registerDataTransferNotification(BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid());

            if(!weakRef.get().getCsBlockSupported())
            {   // Bootloader version 4
                weakRef.get().readCSKeys(OtaUpdateManager.REQUEST_READ_CSKEYS_BT, OtaUpdateManager.USER_KEY_BT_ADDRESS);
                weakRef.get().readCSKeys(OtaUpdateManager.REQUEST_READ_CSKEYS_XLST, OtaUpdateManager.USER_KEY_XTAL_TRIM);
                weakRef.get().readCSKeys(OtaUpdateManager.REQUEST_READ_CSKEYS_IDROOT,OtaUpdateManager.USER_KEY_IDENTITY_ROOT);
                weakRef.get().readCSKeys(OtaUpdateManager.REQUEST_READ_CSKEYS_ENCROOT,OtaUpdateManager.USER_KEY_ENCRYPTION_ROOT);
                weakRef.get().enableOTAMode();
            } else {
                weakRef.get().readNextCsBlock();
            }
        }
    }

    /**
     * Handle Bootloader service attributes
     *
     */
    private void handleBootloaderService() {
        IOtaMessageListener listener = null;
        WeakReference<OtaUpdateManager> weakRef = new WeakReference<OtaUpdateManager>(OtaUpdateManager.getInstance());
        if (weakRef.get() != null)
            listener = weakRef.get().getListener();
        
        if (listener != null)
            listener.onConnectionStateUpdate(BtSmartService.MESSAGE_CONNECTED);
        
        if (weakRef.get() != null) {

            // Handle case for V4/V5
            if(!weakRef.get().getBootloaderSoftwareVersionSupported()) {

                if (weakRef.get().getHostValidation()) {
                    weakRef.get().readDataTransferCharacteristic();
                } else {
                    weakRef.get().registerBootloaderTransferControlNotifiocation();
                    weakRef.get().readBootloadVersion();

                    // This process is for V5 onwards
                    if (weakRef.get().getReadCskeyFromBootloader()) {
                        weakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_BT_FROM_BL,
                                OtaUpdateManager.USER_KEY_BT_ADDRESS);
                        weakRef.get().readDataTransferCharacteristic();
                        weakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_XLST_FROM_BL,
                                OtaUpdateManager.USER_KEY_XTAL_TRIM);
                        weakRef.get().readDataTransferCharacteristic();
                        weakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_IDROOT_FROM_BL,
                                OtaUpdateManager.USER_KEY_IDENTITY_ROOT);
                        weakRef.get().readDataTransferCharacteristic();
                        weakRef.get().readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_ENCROOT_FROM_BL,
                                OtaUpdateManager.USER_KEY_ENCRYPTION_ROOT);
                        weakRef.get().readDataTransferCharacteristic();
                    }

                    if (listener != null)
                        listener.onOtauStarted();
                }
            } else {
                /*This is for non-bonding on-chip applications to trigger temporary pairing*/
                if(!weakRef.get().isBonded()){
                    weakRef.get().readAuthenticatedCharacteristic();
                    //weakRef.get().readTransferControlStatus();
                }
                // Auto detect challenge response setting
                weakRef.get().readTransferControlStatus();
            }
        }
    }

    /**
     * Check services from
     *
     * @param msg
     *           The Android Message object which will be sent out
     */
    private byte checkServices(Message msg){
        WeakReference<OtaUpdateManager> weakRef = new WeakReference<OtaUpdateManager>(OtaUpdateManager.getInstance());
        Bundle bundle = msg.getData();
        ParcelUuid[] ServiceUuidArray = (ParcelUuid[]) bundle.getParcelableArray(BtSmartService.EXTRA_SERVICE_UUID_ARRAY);
        boolean isRefresh = bundle.getBoolean(BtSmartService.EXTRA_REFRESH_ATTRIBUTES);
        boolean isRefreshDone = bundle.getBoolean(BtSmartService.EXTRA_REFRESH_ATTRIBUTES_DONE);
        weakRef.get().setCsBlockSupported(false);
        weakRef.get().setReadCskeyFromBootloader(false);
        weakRef.get().setReadBootloaderSoftwareVersion(false);
        weakRef.get().setRefreshAttributesDone(isRefreshDone);
        if(isRefresh) {
           weakRef.get().refreshAttribute();
           return REFRESH_SERVICE_MODE;
        }

        for (ParcelUuid uuid : ServiceUuidArray) {
            if (uuid.getUuid().compareTo(BtSmartService.BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid()) == 0) {
                ParcelUuid[] CharaUuidArray =(ParcelUuid[]) bundle.getParcelableArray(BtSmartService.EXTRA_CHARACTERISTIC_UUID_ARRAY);
                if (CharaUuidArray != null) {
                    for (ParcelUuid charaUuid : CharaUuidArray) {
                        if (charaUuid.getUuid().compareTo(BtSmartService.BtSmartUuid.OTAU_APPLICATION_READCSBLOCK.getUuid()) == 0) {
                            weakRef.get().setCsBlockSupported(true);
                        }
                        if(charaUuid.getUuid().compareTo(BtSmartService.BtSmartUuid.OTAU_CSR_OTA_VERSION.getUuid()) == 0) {
                            weakRef.get().setCsrOtaVersion(true);
                        }
                    }
                }
                return APPLICATION_SERVICE_MODE;
            }
            else if (uuid.getUuid().compareTo(BtSmartService.BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid()) == 0) {
                ParcelUuid[] CharaUuidArray = (ParcelUuid[]) bundle.getParcelableArray(BtSmartService.EXTRA_CHARACTERISTIC_UUID_ARRAY);
                if (CharaUuidArray != null) {
                    for (ParcelUuid charaUuid : CharaUuidArray) {
                        if (charaUuid.getUuid().compareTo(BtSmartService.BtSmartUuid.OTAU_APPLICATION_READCSKEY.getUuid()) == 0) {
                            weakRef.get().setReadCskeyFromBootloader(true);
                        }
                        if(charaUuid.getUuid().compareTo(BtSmartService.BtSmartUuid.OTAU_BOOTLOADER_SOFTWARE_VERSION.getUuid()) == 0){
                            weakRef.get().setReadBootloaderSoftwareVersion(true);
                        }
                    }
                }
                return BOOTLOADER_SERVICE_MODE;
            }
        }
        
        return UNKNOWN_SERVICE_MODE;
    }
}
