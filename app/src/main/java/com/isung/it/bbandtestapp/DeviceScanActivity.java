/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;

/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class DeviceScanActivity extends ScanResultsActivity {

    public static final String MESH_DEVICE = "MESH_DEVICE";
    @Override
    protected void connectBluetooth(BluetoothDevice deviceToConnect) {
        Intent intent = new Intent(this, OTAFragment.class);
        intent.putExtra(BluetoothDevice.EXTRA_DEVICE, deviceToConnect);
        setResult(RESULT_OK, intent);
        finish();
    }
    @Override
    protected void connectBluetooth(BluetoothDevice deviceToConnect, boolean isMeshDevice) {
        Intent intent = new Intent(this, OTAFragment.class);
        intent.putExtra(BluetoothDevice.EXTRA_DEVICE, deviceToConnect);
        intent.putExtra(MESH_DEVICE,isMeshDevice);
        setResult(RESULT_OK, intent);
        finish();
    }

}