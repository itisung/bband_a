/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

/**
 * This class represents a Pskey entry in the CS key XML. It includes the data members "NAME","ID","OFFSET" and "LENGTH"
 * 
 */
public class CskeyItem {
    public String name;
    public int id;
    public int offset;
    public int length;
    public byte[] value;

    /**
     * The constructor for Pskey entry
     * 
     * @param name
     *            String CsKey name attribute
     * 
     * @param id
     *            int CsKey id attribute
     * 
     * @param offset
     *            int the CsKeyitem offset into the db
     * 
     * @param length
     *            int the length of CsKey value
     */
    public CskeyItem(String name, int id, int offset, int length) {
        this.name = name;
        this.id = id;
        this.offset = offset;
        this.length = length;
        this.value = new byte[length*2];
    }
}