package com.isung.it.bbandtestapp;

import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.NumberFormat;

/**
 * Fragment that appears in the "content_frame", shows a planet
 */
public class TimerSettingFragment extends Fragment {
    private static final String TAG = TimerSettingFragment.class.getSimpleName();

    public static final String ARG_PLANET_NUMBER = "planet_number";

    private static final int HANDLER_INTERFACE_AUTO_ON_TIME_SETTING  = 1;
    private static final int HANDLER_INTERFACE_AUTO_OFF_TIME_SETTING = 2;
    private static final int HANDLER_INTERFACE_AUTO_RESERVATION = 3;

    private static final int DEFAULT_TIME_ON_HOUR = 07;
    private static final int DEFAULT_TIME_ON_MIN = 00;
    private static final int DEFAULT_TIME_OFF_HOUR = 23;
    private static final int DEFAULT_TIME_OFF_MIN = 00;

    private static final int HANDLER_UI_SWITCH = 10;

    private DeviceActivity mContext = null;

    private int mOnHour = DEFAULT_TIME_ON_HOUR;
    private int mOnMinute = DEFAULT_TIME_ON_MIN;
    private int mOffHour = DEFAULT_TIME_OFF_HOUR;
    private int mOffMinute = DEFAULT_TIME_OFF_MIN;

    private int mOnHourTemp = 00;
    private int mOnMinuteTemp = 00;
    private int mOffHourTemp = 00;
    private int mOffMinuteTemp = 00;

    private Button mSaveBtn, mCancelBtn;
    private TextView mOnTimeData, mOffTimeData;
    private Switch mTimeSwitch;
    private TimePickerDialog mOnTPDialog, mOffTPDialog;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch (msg.what) {
                case HANDLER_INTERFACE_AUTO_ON_TIME_SETTING:{
                    Log.d(TAG, "HANDLER_INTERFACE_AUTO_ON_TIME_SETTING");
                    mContext.sendBLEOnTimeSetting(mOnHourTemp, mOnMinuteTemp);
                    mContext.sendBLEAutoTimeRequest();
                    break;
                }
                case HANDLER_INTERFACE_AUTO_OFF_TIME_SETTING:{
                    Log.d(TAG, "HANDLER_INTERFACE_AUTO_OFF_TIME_SETTING");
                    mContext.sendBLEOffTimeSetting(mOffHourTemp, mOffMinuteTemp);
                    mContext.sendBLEAutoTimeRequest();
                    break;
                }
                case HANDLER_INTERFACE_AUTO_RESERVATION:{
                    break;
                }
            }
        }
    };

    private Runnable mMyTask = new Runnable() {
        @Override
        public void run() {
            Log.d("mymy", "run run");

            if(mTimeSwitch != null){
                mTimeSwitch.setEnabled(true);
            }
        }
    };

    public TimerSettingFragment() {
        // Empty constructor required for fragment subclasses
    }

    public static Fragment newInstance() {
        Fragment fragment = new TimerSettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = (DeviceActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_timer_setting, container, false);


        mContext.getActionBar().setTitle("타이머");

        mSaveBtn = (Button)rootView.findViewById(R.id.save_btn);
        mOnTimeData = (TextView)rootView.findViewById(R.id.on_time_data);
        mOffTimeData = (TextView)rootView.findViewById(R.id.off_time_data);
        mTimeSwitch = (Switch)rootView.findViewById(R.id.time_switch);
        mCancelBtn = (Button)rootView.findViewById(R.id.cancel_btn);

        mSaveBtn.setOnClickListener(mOnClickListener);
        mOnTimeData.setOnClickListener(mOnClickListener);
        mOffTimeData.setOnClickListener(mOnClickListener);
        mTimeSwitch.setOnClickListener(mOnClickListener);
        mTimeSwitch.setOnTouchListener(mOnTouchListener);

        mCancelBtn.setOnClickListener(mOnClickListener);

        SharedPreferences activityPrefs = BLEApiUtils.getPreferences(mContext);
        mOnHour = activityPrefs.getInt(Preferences.SETTING_ON_TIME_HOUR, 99);
        mOnMinute = activityPrefs.getInt(Preferences.SETTING_ON_TIME_MIN, 99);
        mOffHour = activityPrefs.getInt(Preferences.SETTING_OFF_TIME_HOUR, 99);
        mOffMinute = activityPrefs.getInt(Preferences.SETTING_OFF_TIME_MIN, 99);

        initTimeView();

        //스위치 ui 갱신
        int mIsUseCheck = activityPrefs.getInt(Preferences.SETTING_IS_TIME_RESERVATION_USE, 0);
        if(mIsUseCheck == 1){
            mTimeSwitch.setChecked( true );
        } else {
            mTimeSwitch.setChecked( false );
        }

//        int imageId = getResources().getIdentifier(planet.toLowerCase(Locale.getDefault()),
//                "drawable", getActivity().getPackageName());
//        ImageView iv = ((ImageView) rootView.findViewById(R.id.image));
//        iv.setImageResource(imageId);

        return rootView;
    }

    private void initTimeView(){
        NumberFormat numformat = NumberFormat.getIntegerInstance();
        numformat.setMinimumIntegerDigits(2);

        Log.d(TAG,"mOnHour = " + mOnHour + " mOnMinute = " + mOnMinute + " mOffHour = " + mOffHour + " mOffMinute = " + mOffMinute);

        if(mOnTimeData != null && mOnHour != 99 && mOnMinute != 99){
            mOnTimeData.setText(numformat.format(mOnHour) + " : " + numformat.format(mOnMinute));
        } else {
            mOnTimeData.setText("-- : --");
        }

        if(mOffTimeData != null && mOffHour != 99 && mOffMinute != 99){
            mOffTimeData.setText(numformat.format(mOffHour) + " : " + numformat.format(mOffMinute));
        } else {
            mOffTimeData.setText("-- : --");
        }
    }

    /**
     * BT와 통신한 결과 값이 이곳으로 들어온다.
     * @param message
     */
    public void BLEReadMessage(byte[] message){
        Log.d(TAG, "BLEReadMessage()");

        StringBuilder sb = new StringBuilder(message.length * 2);
        for( byte b : message) {
            sb.append(String.format("%02x", b & 0xff)).append(" ");
        }
        Log.d(TAG, "BLEReadMessage data : " + sb);

        //TODO CRC 체크 필요

        if(message != null && message.length >= 2){
            switch (message[1]){
                case (byte)BLEApiUtils.BLE_API_ON_TIME_SETTING: {
                    Log.d(TAG, "BLE_API_ON_TIME_SETTING");
                    mOnHour = mOnHourTemp;
                    mOnMinute = mOnMinuteTemp;
                    initTimeView();

                    //on 시간값 저장
                    if(mContext != null){
                        SharedPreferences activityPrefs = BLEApiUtils.getPreferences(mContext);
                        SharedPreferences.Editor editor = activityPrefs.edit();
                        editor.putInt(Preferences.SETTING_ON_TIME_HOUR, mOnHour);
                        editor.putInt(Preferences.SETTING_ON_TIME_MIN, mOnMinute);
                        editor.commit();
                    }
                    break;
                }
                case (byte)BLEApiUtils.BLE_API_OFF_TIME_SETTING: {
                    Log.d(TAG, "BLE_API_OFF_TIME_SETTING");
                    mOffHour = mOffHourTemp;
                    mOffMinute = mOffMinuteTemp;
                    initTimeView();

                    //off 시간값 저장
                    if(mContext != null){
                        SharedPreferences activityPrefs = BLEApiUtils.getPreferences(mContext);
                        SharedPreferences.Editor editor = activityPrefs.edit();
                        editor.putInt(Preferences.SETTING_OFF_TIME_HOUR, mOffHour);
                        editor.putInt(Preferences.SETTING_OFF_TIME_MIN, mOffMinute);
                        editor.commit();
                    }
                    break;
                }
                //TODO 테스트 코드 삭제 필요
                case (byte)BLEApiUtils.BLE_API_AUTO_ON_OFF_TIME_REQUEST: {
                    Log.d(TAG, "BLE_API_AUTO_ON_OFF_TIME_REQUEST");

                    mOnHour = mOnHourTemp;
                    mOnMinute = mOnMinuteTemp;
                    initTimeView();
                    break;
                }
                case (byte)BLEApiUtils.BLE_API_AUTO_ON_OFF_SETTING: {
                    Log.d(TAG, "BLE_API_AUTO_ON_OFF_SETTING");

                    if(mTimeSwitch != null){
                        if(mHandler != null){
                            mHandler.removeCallbacks(mMyTask);
                        }
                        mTimeSwitch.setEnabled(true);

                        int mIsUseCheck = 0;
                        if(mTimeSwitch.isChecked() == true){
                            mIsUseCheck = 0;
                            mTimeSwitch.setChecked( false );
                        } else {
                            mIsUseCheck = 1;
                            mTimeSwitch.setChecked( true );
                        }

                        //스위치의 상태값 저장
                        if(mContext != null){
                            SharedPreferences activityPrefs = BLEApiUtils.getPreferences(mContext);
                            SharedPreferences.Editor editor = activityPrefs.edit();
                            editor.putInt(Preferences.SETTING_IS_TIME_RESERVATION_USE, mIsUseCheck);
                            editor.commit();
                        }
                    }

                    break;
                }
                case (byte)BLEApiUtils.BLE_API_GET_VERSION: {
                    Log.d(TAG, "BLE_API_GET_VERSION");

                    break;
                }

                //예약시간 및 사용여부 값이 들어오면 ui값을 갱신한다.
                case (byte) BLEApiUtils.BLE_API_TIME_RESERVATION_CONFIRM: {
                    Log.d(TAG, "BLE_API_TIME_RESERVATION_CONFIRM");

                    if(message.length >= 7){
                        mOnHour = message[3];
                        mOnMinute = message[4];
                        mOffHour = message[5];
                        mOffMinute = message[6];

                        initTimeView();
                    }

                    if(message.length >= 8){
                        int mIsUseCheck = message[7];

                        if(mTimeSwitch != null){
                            if(mIsUseCheck == 0x01){
                                mTimeSwitch.setChecked(true);
                            } else {
                                mTimeSwitch.setChecked(false);
                            }
                        }
                    }

                    break;
                }
            }
        }
    }

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener(){
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch( v.getId() ) {
                case R.id.time_switch: {
                    if(mOnHour == 99 || mOnMinute == 99 || mOffHour == 99 || mOffMinute == 99){
                        if( mContext != null){
                            Toast.makeText(mContext, "시간 설정을 하신 후\n예약 기능을 사용해 주세요.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (mTimeSwitch != null) {
                            mTimeSwitch.setEnabled(false);
                        }

                        if(mHandler != null){
                            mHandler.postDelayed(mMyTask, 700);
                        }

                        boolean isCheck = false;
                        if (mTimeSwitch != null && mTimeSwitch.isChecked() == true) {
                            isCheck = false;
                        } else {
                            isCheck = true;
                        }
                        mContext.sendBLEAutoOnOffSetting(isCheck);
                    }
                }
            }
            return true;
        }
    };


    private View.OnClickListener mOnClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            switch( v.getId() ) {
                case R.id.save_btn: {
                    Log.d(TAG, "case R.id.save_btn:");
                    Toast.makeText(mContext, "저장", Toast.LENGTH_SHORT).show();
                    mContext.contentFrameGone();
                    break;
                }
                case R.id.on_time_data: {
                    Log.d(TAG, "case R.id.on_time_data:");

                    int onHour = 0;
                    int onMinute = 0;
                    if(mOnHour == 99){
                        onHour = DEFAULT_TIME_ON_HOUR;
                    } else {
                        onHour = mOnHour;
                    }
                    if(mOnMinute == 99){
                        onMinute = DEFAULT_TIME_ON_MIN;
                    } else {
                        onMinute = mOnMinute;
                    }

                    mOnTPDialog = new TimePickerDialog(getActivity(), mOnTimeSetListener, onHour, onMinute, false);
                    if(mOnTPDialog != null && mOnTPDialog.isShowing() == false){
                        mOnTPDialog.show();
                    }
                    break;
                }
                case R.id.off_time_data: {
                    Log.d(TAG, "case R.id.off_time_data:");

                    int offHour = 0;
                    int offMinute = 0;
                    if(mOffHour == 99){
                        offHour = DEFAULT_TIME_OFF_HOUR;
                    } else {
                        offHour = mOffHour;
                    }
                    if(mOffMinute == 99){
                        offMinute = DEFAULT_TIME_OFF_MIN;
                    } else {
                        offMinute = mOffMinute;
                    }

                    mOffTPDialog = new TimePickerDialog(getActivity(), mOffTimeSetListener, offHour, offMinute, false);
                    if(mOffTPDialog != null && mOffTPDialog.isShowing() == false){
                        mOffTPDialog.show();
                    }
                    break;
                }
                case R.id.cancel_btn: {
                    Log.d(TAG, "case R.id.cancel_btn:");
                    mContext.contentFrameGone();
                    break;
                }
            }
        }
    };

    private TimePickerDialog.OnTimeSetListener mOnTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if(mOnHour == hourOfDay && mOnMinute == minute){
                return;
            }

            if (null != mHandler) {
                mOnHourTemp = hourOfDay;
                mOnMinuteTemp = minute;

                mHandler.sendEmptyMessage(HANDLER_INTERFACE_AUTO_ON_TIME_SETTING);
                Log.d(TAG, "HANDLER_INTERFACE_AUTO_ON_TIME_SETTING : " + hourOfDay + " : " +minute);
            }
        }
    };

    private TimePickerDialog.OnTimeSetListener mOffTimeSetListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if (null != mHandler) {

                if(mOffHour == hourOfDay && mOffMinute == minute){
                    return;
                }

                mOffHourTemp = hourOfDay;
                mOffMinuteTemp = minute;

                mHandler.sendEmptyMessage(HANDLER_INTERFACE_AUTO_OFF_TIME_SETTING);
                Log.d(TAG, "HANDLER_INTERFACE_AUTO_OFF_TIME_SETTING : " + hourOfDay + " : " +minute);
            }
        }
    };

}