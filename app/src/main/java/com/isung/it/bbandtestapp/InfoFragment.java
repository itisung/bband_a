package com.isung.it.bbandtestapp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment that appears in the "content_frame", shows a planet
 */
public class InfoFragment extends Fragment {
    public static final String ARG_PLANET_NUMBER = "planet_number";

    public InfoFragment() {
        // Empty constructor required for fragment subclasses
    }

    public static Fragment newInstance(int position) {
        Fragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putInt(InfoFragment.ARG_PLANET_NUMBER, position);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_info, container, false);
        int i = getArguments().getInt(ARG_PLANET_NUMBER);
        String planet = getResources().getStringArray(R.array.planets_array)[i];

//        int imageId = getResources().getIdentifier(planet.toLowerCase(Locale.getDefault()),
//                "drawable", getActivity().getPackageName());
//        ImageView iv = ((ImageView) rootView.findViewById(R.id.image));
//        iv.setImageResource(imageId);

        getActivity().getActionBar().setTitle(planet);
        return rootView;
    }
}