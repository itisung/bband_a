/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.csr.btsmart.BtSmartService;
import com.csr.btsmart.BtSmartService.BtSmartUuid;
import com.isung.it.bbandtestapp.model.State.ReadCsBlockState;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class OtaUpdateManager {

    public final static String USER_KEYS_CONFIGURATION = Environment.getExternalStorageDirectory().getPath() + "/csr/userkey.conf";

    // OTA request message id
    public static final int REQUEST_SERVICE_CHANGED = 1;
    public static final int REQUEST_DATATRANSFER_NOTIFICATION = 2;
    public static final int REQUEST_READ_CSKEYS_BT = 3;
    public static final int REQUEST_READ_CSKEYS_XLST = 4;
    public static final int REQUEST_READ_CSKEYS_IDROOT = 5;
    public static final int REQUEST_READ_CSKEYS_ENCROOT = 6;
    public static final int REQUEST_ENABLE_OTA_MODE = 7;
    public static final int REQUEST_BL_TRANSFERCONTROL_NOTIFICATION = 8;
    public static final int REQUEST_READ_DATA_TRANSFER = 9;
    public static final int REQUEST_WRITE_DATA_TRANSFER = 10;
    public static final int REQUEST_READ_BOOTLOADER_VERSION = 11;
    public static final int REQUEST_SET_APPLCATION_ONCHIP = 12;
    public static final int REQUEST_GET_APPLCATION_ONCHIP = 13;
    public static final int REQUEST_SET_TRANSFER_CONTROL_READY = 14;
    public static final int REQUEST_SET_TRANSFER_CONTROL_INPROGRESS = 15;
    public static final int REQUEST_WRITE_BYTES_TO_TARGET = 16;
    public static final int REQUEST_SET_TRANSFER_CONTROL_COMPLETE = 17;
    public static final int REQUEST_PAUSE_DOWNLOAD = 18;
    public static final int REQUEST_CANCEL_DOWNLOAD = 19;
    public static final int REQUEST_READ_CSBLOCK = 20;
    public static final int REQUEST_READ_CSKEYS_BT_FROM_BL = 21;
    public static final int REQUEST_READ_CSKEYS_XLST_FROM_BL = 22;
    public static final int REQUEST_READ_CSKEYS_IDROOT_FROM_BL = 23;
    public static final int REQUEST_READ_CSKEYS_ENCROOT_FROM_BL = 24;
    public static final int REQUEST_READ_TRANSFER_CONTROL_STATUS = 25;
    public static final int REQUEST_SET_TRANSFER_CONTROL_FAILED = 26;
    public static final int REQUEST_READ_BL_SOFTWARE_VERSION    = 27;
    public static final int REQUEST_READ_CSR_OTA_VERSION    = 28;
    public static final int REQUEST_READ_DEVICE_SOFTWARE_VERSION    = 29;
    public static final int REQUEST_READ_AUTHENTICATED_CHARACTERISTIC = 30;

    // These are boot loader transfer control value
    public static final short SET_TRANSFER_CONTROL_INIT = 0;
    public static final short SET_TRANSFER_CONTROL_READY = 1;
    public static final short SET_TRANSFER_CONTROL_INPROGRESS = 2;
    public static final short SET_TRANSFER_PAUSED = 3;
    public static final short SET_TRANSFER_COMPLETED = 4;
    public static final short SET_TRANSFER_FAILED = 5;
    public static final short SET_TRANSFER_ABORT = 6;

    // These are read CS keys value
    public final static int USER_KEY_BT_ADDRESS = 0x01;
    public final static int USER_KEY_XTAL_TRIM = 0x02;
    public final static int USER_KEY_IDENTITY_ROOT = 0x11;
    public final static int USER_KEY_ENCRYPTION_ROOT = 0x12;

    // These are error message ids
    public static final int ERROR_MESSAGE_ID_READCSKEY_BTADDRESS = 1;
    public static final int ERROR_MESSAGE_ID_READCSKEY_XLST = 2;
    public static final int ERROR_MESSAGE_ID_READCSKEY_IDROOT = 3;
    public static final int ERROR_MESSAGE_ID_READCSKEY_ENCROOT = 4;
    public static final int ERROR_MESSAGE_ID_CHALLENGE_RESPONSE = 5;
    public static final int ERROR_MESSAGE_ID_ENABLE_OTA = 6;
    public static final int ERROR_MESSAGE_ID_WRITE_TO_TARGET = 7;
    public static final int ERROR_MESSAGE_ID_BL_TRANSFERCONTROL_NOTIFICATION = 8;
    public static final int ERROR_MESSAGE_ID_NULL_CHARACTERISTIC = 9;
    public static final int ERROR_MESSAGE_ID_NULL_SERVICE = 10;
    public static final int ERROR_MESSAGE_ID_SET_TRANSFER_CONTROL_INPROGRESS = 11;
    public static final int ERROR_MESSAGE_ID_SET_APPLICATION_ID = 12;

    // These are CS Keys offset and size definition
    public static final byte READ_CSKEY_BUILDID_OFFSET = 0;
    public static final byte READ_CSKEY_BUILDID_LENGTH = 2;

    final static int DATA_LENGTH = 20;
    private final static int CONNECTION_DELAY_TIME_MS      = 200;

    private static OtaUpdateManager mInstance;
    private BtSmartService mGattClient = null;
    private BluetoothDevice mBLEDevice = null;
    private OtaMessageHandler mStatemachine;
    private ArrayList<CskeyItem> mCskeyList;
    private boolean mCsBlockSupported = false;
    private boolean mReadCskeyFromBootloader = false;
    private boolean mSoftwareVersionSupported = false;
    private boolean mCsrOtaVersionSupported = false;

    private int mProgressing = 0;
    private byte[] mImageData;
    private IOtaMessageListener mListener;
    private boolean isDownloadpaused = false;
    private int mSentNum = 0;
    private boolean isAesEncryptionEnabled = false;
    private ArrayList<CskeyItem> mMergedCsKeyList;
    private boolean mIsCancelDownload = false;
    private boolean mRefreshAttributesDone = false;
    private boolean mIsMeshdevice = false;

    static byte[] mSharedSecretKeySdk = new byte[] { (byte) 0x00, (byte) 0x11, (byte) 0x22, (byte) 0x33, (byte) 0x44,
            (byte) 0x55, (byte) 0x66, (byte) 0x77, (byte) 0x88, (byte) 0x99, (byte) 0xaa, (byte) 0xbb, (byte) 0xcc,
            (byte) 0xdd, (byte) 0xee, (byte) 0xff };

    /**
     * Callbacks for changes to the state of the connection to BtSmartService.
     */
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mGattClient = ((BtSmartService.LocalBinder) rawBinder).getService();
        }

        public void onServiceDisconnected(ComponentName classname) {
            mGattClient = null;
        }
    };

    /**
     * This is the handler for general connection messages.
     */
    private ConnectionHandler mDeviceHandler;

    /*
     * make default constructor invisible
     */
    private OtaUpdateManager() {
    }

    /*
     * Initialising OTA update manager
     * 
     * @param context Activity context
     */
    public static void initialize(Context context) {

        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            return;
        }

        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null) {
            return;
        }

        mInstance = new OtaUpdateManager();
        mInstance.mStatemachine = new OtaMessageHandler("OTA Update StateMachine");
        mInstance.mDeviceHandler = new ConnectionHandler("OTA Connection Handler");

        Intent bindIntent = new Intent(context, BtSmartService.class);
        bindIntent.putExtra(BtSmartService.EXTRA_DISCOVER_SERVICES_DELAY_MS, CONNECTION_DELAY_TIME_MS);
        context.bindService(bindIntent, mInstance.mServiceConnection, Context.BIND_AUTO_CREATE);

        State.setState(State.ConnectionState.STATE_IDLE);
        State.setScanState(State.ScanState.STATE_SCAN_IDLE);
        State.setOtaState(State.OtaState.STATE_OTA_IDLE);
        State.setReadCsBlockState(State.ReadCsBlockState.READ_CS_BLOCK_BUILD_ID);
        mInstance.mMergedCsKeyList = new ArrayList<CskeyItem>();
        mInstance.mIsCancelDownload = false;
    }

    /*
     * Get OTA update manage singleton instance
     */
    public static OtaUpdateManager getInstance() {
        return mInstance;
    }

    /*
     * Destroy this instance
     * 
     * @param context Activity context
     */
    public void destroy(Context context) {
        if (mInstance != null) {
            context.unbindService(mServiceConnection);
            Intent bindIntent = new Intent(context, BtSmartService.class);
            context.stopService(bindIntent);
            if(mGattClient != null){
                mGattClient.disconnect();
                mGattClient = null;
            }
        }
    }

    /*
     * Host application acts as Gatt client to connect BLE device
     * 
     * @param device BluetoothDevice The BLE device instance
     * 
     * @param listener IOtaMessageListener The listener who are interested in OTA message
     */
    public void connect(BluetoothDevice device, IOtaMessageListener listener) {
        if (device != null && mDeviceHandler != null && mGattClient != null) {
            mBLEDevice = device;
            mListener = listener;
            mGattClient.connectAsClient(mBLEDevice, mDeviceHandler);
        }
    }

    /*
     * Provide interface for reconnect to BLE devices after switching to bootloader mode
     */
    public void connect() {
        if (mBLEDevice != null && mDeviceHandler != null && mGattClient != null) {
            mGattClient.connectAsClient(mBLEDevice, mDeviceHandler);
        }
    }

    public void connect(final boolean refresh) {
        if (mBLEDevice != null && mDeviceHandler != null && mGattClient != null) {
            if(refresh){
                if(mListener != null){
                    mListener.onOtaStateUpdate(State.OtaState.STATE_OTA_RECONNECT_GATT);
                }
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mGattClient.connectAsClient(mBLEDevice, mDeviceHandler, refresh);
                    }
                }, 1500);
            }
        }
    }

    /*
     * Disconnect BLE devices
     */
    public void disconnect() {
        if (mGattClient != null) {
            mGattClient.disconnect();
        }
    }

    /*
     * Tell host application if remote application support challenge response
     */
    public void setHostValidation(boolean enable) {
        isAesEncryptionEnabled = enable;
    }

    /*
     * Check if remote device supports challenge response
     */
    public boolean getHostValidation() {
        return isAesEncryptionEnabled;
    }

    IOtaMessageListener getListener() {
        return mListener;
    }

    /**
     * read characteristic to get Boot loader version
     */
    public void readBootloadVersion() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_READ_BOOTLOADER_VERSION,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(), BtSmartUuid.OTAU_CSR_OTA_VERSION.getUuid(),
                    mStatemachine.getHandler());
        }
    }

    public void readBootloaderSoftwareVersion() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_READ_BL_SOFTWARE_VERSION,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(), BtSmartUuid.OTAU_BOOTLOADER_SOFTWARE_VERSION.getUuid(),
                    mStatemachine.getHandler());
        }
    }

    public void readCsrOtaVersion() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_READ_CSR_OTA_VERSION,
                    BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid(), BtSmartUuid.OTAU_CSR_OTA_VERSION.getUuid(),
                    mStatemachine.getHandler());
        }
    }

    public void getSoftwareVersion() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_READ_DEVICE_SOFTWARE_VERSION,
                    BtSmartUuid.DEVICE_INFORMATION_SERVICE.getUuid(), BtSmartUuid.SOFTWARE_REVISION.getUuid(),
                    mStatemachine.getHandler());
        }
    }

    /**
     * Read characteristic to get CS keys, e.g BT address ,crystal trim value,Identity Root,Encryption Root
     * 
     * @param requestId
     *            The request id
     * 
     * @Param key The index of user key
     */
    public void readCSKeys(int requestId, int key) {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(requestId, BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_APPLICATION_READCSKEY.getUuid(), new byte[]{(byte) key},
                    mStatemachine.getHandler());
        }
    }

    /**
     * Read CS keys from the Bootloader service, e.g BT address and crystal trim value
     * 
     * @param requestId
     *            The request id
     * 
     * @param block
     *            CS keys blocks
     */
    public void readCSKeysFromBootloaderService(int requestId, int key) {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(requestId, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_APPLICATION_READCSKEY.getUuid(), new byte[]{(byte) key},
                    mStatemachine.getHandler());
        }
    }

    /**
     * Read characteristic to get CS Block, e.g BT address and crystal trim value
     * 
     * @param block
     *            CS keys blocks
     */
    public void readNextCsBlock() {

        byte data[] = null;

        ReadCsBlockState state = State.getReadCsBlockState();
        if (state == null) {
            State.setReadCsBlockState(ReadCsBlockState.READ_CS_BLOCK_BUILD_ID);
            mCskeyList.clear();
            mCskeyList = null;

            if(mIsCancelDownload){
                disconnect();
                mIsCancelDownload = false;
                return;
            }

            if(mListener != null)
                mListener.onOtauEnabled(true);
            return;
        }

        data = new byte[4];
        if (state == ReadCsBlockState.READ_CS_BLOCK_BUILD_ID) {
            data[0] = (byte) ((READ_CSKEY_BUILDID_OFFSET >>> 0) & 0xFF);
            data[1] = (byte) ((READ_CSKEY_BUILDID_OFFSET >>> 8) & 0xFF);
            data[2] = (byte) ((READ_CSKEY_BUILDID_LENGTH >>> 0) & 0xFF);
            data[3] = (byte) ((READ_CSKEY_BUILDID_LENGTH >>> 8) & 0xFF);
        }
        else {
            if (mCskeyList == null)
                return;
            
            boolean isFound = false;
            for (CskeyItem cskey : mCskeyList) {
                if (cskey.id == state.valueOf()) {
                    data[0] = (byte) ((cskey.offset >>> 0) & 0xFF);
                    data[1] = (byte) ((cskey.offset >>> 8) & 0xFF);
                    data[2] = (byte) ((cskey.length * 2 >>> 0) & 0xFF);
                    data[3] = (byte) ((cskey.length * 2 >>> 8) & 0xFF);
                    isFound = true;
                    break;
                }
            }

            if(!isFound){
                State.setReadCsBlockState(state.getNext());
                readNextCsBlock();
                return;
            }
        }

        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_READ_CSBLOCK, BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_APPLICATION_READCSBLOCK.getUuid(), data, mStatemachine.getHandler());

            State.setOtaState(State.OtaState.STATE_OTA_INIT_READ_CS_BLOCK);
        }
    }
    
    public void setNextReadCsBlockState() {
        ReadCsBlockState state = State.getReadCsBlockState();
        State.setReadCsBlockState(state.getNext());
    }

    /**
     * Set current application id for the remote device
     * 
     * @param idx
     *            The index of the application
     */
    public void setApplicationOnChip(int idx) {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_SET_APPLCATION_ONCHIP, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_CURRENT_APPLICATION.getUuid(), new byte[] { (byte) idx },
                    mStatemachine.getHandler());
        }

    }

    /**
     * Read an authenticated characteristic in application service in order to trigger pairing procedure
     *
     */
    public void readAuthenticatedCharacteristicInApp() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_GET_APPLCATION_ONCHIP, BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_CURRENT_APPLICATION.getUuid(), mStatemachine.getHandler());
        }

    }

    public boolean isBonded(){
        if(mBLEDevice != null)
            return mBLEDevice.getBondState() == BluetoothDevice.BOND_BONDED;
        else
            return false;
    }

    /**
     * Write characteristic to get BLE device ready for downloading
     */
    public void setTransferControlReady() {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_SET_TRANSFER_CONTROL_READY,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(),
                    new byte[] { (byte) SET_TRANSFER_CONTROL_READY }, mStatemachine.getHandler());
        }
    }

    /**
     * Write characteristic to get BLE device into downloading in progress status
     */
    public void setTransferControlInProgress() {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_SET_TRANSFER_CONTROL_INPROGRESS,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(),
                    new byte[] { (byte) SET_TRANSFER_CONTROL_INPROGRESS }, mStatemachine.getHandler());
        }
    }

    /**
     * Write characteristic to inform BLE device downloading completed
     */
    private void setTransferComplete() {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_SET_TRANSFER_CONTROL_COMPLETE,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(),
                    new byte[] { (byte) SET_TRANSFER_COMPLETED }, mStatemachine.getHandler());

        }
    }

    /**
     * Write characteristic to inform BLE device OTAU failed
     */
    public void setTransferFailed() {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_SET_TRANSFER_CONTROL_FAILED,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(),
                    new byte[] { (byte) SET_TRANSFER_FAILED }, mStatemachine.getHandler());
        }
    }

    /**
     * write characteristic to BLE device to pause downloading image action
     */
    public void pauseDownload() {
        if (mGattClient != null) {
            pausedDownload(true);
            byte[] data = new byte[] { (byte) SET_TRANSFER_PAUSED};
            mGattClient.writeCharacteristicValue(REQUEST_PAUSE_DOWNLOAD, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(), data, mStatemachine.getHandler());
        }
    }

    /**
     * Write characteristic to BLE device to cancel downloading image action
     */
    public void cancelDownload() {
        if (mGattClient != null) {
            mIsCancelDownload = true;
            byte[] data = new byte[] { (byte) SET_TRANSFER_ABORT };
            mGattClient.writeCharacteristicValue(REQUEST_CANCEL_DOWNLOAD, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(), data, mStatemachine.getHandler());
        }
    }

    /**
     * Write characteristic to get current on-chip application id
     */
    public void getApplcationOnChip() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_GET_APPLCATION_ONCHIP,
                    BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid(), BtSmartUuid.OTAU_CURRENT_APPLICATION.getUuid(),
                    mStatemachine.getHandler());
        }
    }

    /**
     * Read Data Transfer characteristic
     */
    public void readDataTransferCharacteristic() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_READ_DATA_TRANSFER, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_DATA_TRANSFER.getUuid(), mStatemachine.getHandler());
        }
    }

    /**
     * Trigger temporary pairing with the target in bootloader mode
     */
    public void readAuthenticatedCharacteristic() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_READ_AUTHENTICATED_CHARACTERISTIC, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_DATA_TRANSFER.getUuid(), mStatemachine.getHandler());
        }
    }

    /**
     * Read transfer control status in Bootloader service
     */
    public void readTransferControlStatus() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicValue(REQUEST_READ_TRANSFER_CONTROL_STATUS,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(), mStatemachine.getHandler());
        }
    }

    /**
     * Read transfer control status in Bootloader service
     */
    public void writeDataTransferCharacteristic(byte[] data) {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_WRITE_DATA_TRANSFER, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_DATA_TRANSFER.getUuid(), data, mStatemachine.getHandler());
        }
    }

    /**
     * Switch to Bootloader service mode
     */
    public void enableOTAMode() {
        if (mGattClient != null) {
            mGattClient.writeCharacteristicValue(REQUEST_ENABLE_OTA_MODE, BtSmartUuid.OTAU_APPLICATION_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_CURRENT_APPLICATION.getUuid(), new byte[] { 0 }, mStatemachine.getHandler());
        } else {
            Log.w("OtaUpdateManager", "null gatt client");
        }
    }

    /**
     * Register Service Changes indication from BLE device
     * 
     */
    public void registerServiceChangedIndication() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicIndication(REQUEST_SERVICE_CHANGED, BtSmartUuid.GATT_SERVICE.getUuid(),
                    BtSmartUuid.SERVICE_CHANGED.getUuid(), mStatemachine.getHandler());
        }
    }

    /**
     * Register Transfer data transfer notification from BLE device
     */
    public void registerDataTransferNotification(UUID serviceUuid) {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicNotification(REQUEST_DATATRANSFER_NOTIFICATION, serviceUuid,
                    BtSmartUuid.OTAU_DATA_TRANSFER.getUuid(), mStatemachine.getHandler());
        }
    }

    /**
     * Register Transfer control notification from BLE device
     */
    public void registerBootloaderTransferControlNotifiocation() {
        if (mGattClient != null) {
            mGattClient.requestCharacteristicNotification(REQUEST_BL_TRANSFERCONTROL_NOTIFICATION,
                    BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                    BtSmartUuid.OTAU_BOOTLOADER_TRANSFER_CONTROL.getUuid(), mStatemachine.getHandler());
        }
    }

    /**
     * Merge CS keys into updated image
     * 
     * @param data
     *            byte[] original image data
     * 
     * @param btAddress
     *            String Bluetooth address
     * 
     * @param xlst
     *            int Crystal trim value
     * 
     * @param idRoot
     *            String 32-bit Identity Root value
     * 
     * @param encRoot
     *            String 32-bit Encryption Root value
     * 
     * @return boolean return true if merging is successful
     */
    public boolean setImageData(byte[] data, String btAddress, int xlst, String idRoot, String encRoot) {
        if (data.length <= 0)
            return false;
        try {
            if(mMergedCsKeyList.size() > 0){
                mImageData = mergeKeys(data, mMergedCsKeyList);
                mMergedCsKeyList.clear();
            }else{
                mImageData = mergeKeys(data, btAddress, xlst, idRoot, encRoot);
            }
            
            mProgressing = 0;
        }
        catch (IncorrectImageException e) {
            e.printStackTrace();
            return false;
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
        catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Host application sends 20-bytes chunk data to BLE device
     * 
     */
    public void sendNextImageChunk() {
        if (mProgressing >= mImageData.length) {
            setTransferComplete();
            mSentNum = 0;
            return;
        }
        else {
            if (!isDownloadpaused) {
                mSentNum = writeBytesToTarget();
            }
        }
    }

    /**
     * Increase progress index if each chunk data is sent out
     * 
     */
    public void increment() {
        if (mSentNum > 0)
            mProgressing += mSentNum;
    }

    /**
     * Reset the number of sending data
     * 
     */
    public void resetSentNum() {
        mSentNum = 0;
    }

    /**
     * Host application pause current downloading
     * 
     */
    public void pausedDownload(boolean paused) {
        isDownloadpaused = paused;
    }

    /**
     * Write characteristic to send out 20-byte chunk data
     * 
     */
    private int writeBytesToTarget() {
        Log.d("OtaUpdateManager","Writing image data to the target");
        int length = DATA_LENGTH;
        if ((mProgressing + DATA_LENGTH) > mImageData.length) {
            length = mImageData.length - mProgressing;
        }

        final byte[] data = new byte[length];

        System.arraycopy(mImageData, mProgressing, data, 0, length);

        if (mGattClient != null) {
            Log.d("OtaUpdateManager","Begin to Write 20 bytes");
                mGattClient.writeCharacteristicValue(REQUEST_WRITE_BYTES_TO_TARGET, BtSmartUuid.OTAU_BOOTLOADER_SERVICE.getUuid(),
                        BtSmartUuid.OTAU_DATA_TRANSFER.getUuid(), data, mStatemachine.getHandler());
            return length;
        }
        else {
            Log.w("OtaUpdateManager", "Gatt client object is null");
            return -1;
        }
    }

    /**
     * Update downloading progress
     * 
     */
    void updateProgressbar() {
        if (mListener != null) {
            mListener.onOtaProgressUpdate(mProgressing * 100 / mImageData.length, mImageData.length, mProgressing);
        }
    }

    /**
     * Generate challenge response key,the mode of operation is Electronic Code Book,
     * and only one block is encrypted at a time.
     * 
     * @param value
     *            byte[] The challenge key from BLE devices
     * 
     * @return byte[] The response key
     */
    byte[] encryptData(byte[] value) {
        try {
            SecretKey aesKey = new SecretKeySpec(mSharedSecretKeySdk, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(value);
            return encrypted;
        }
        catch (GeneralSecurityException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Merge CS keys into updated image
     * 
     * @param data
     *            byte[] original image data
     * 
     * @param btAddress
     *            String Bluetooth address
     * 
     * @param xlst
     *            int Crystal trim value
     * 
     * @param idRoot
     *            String 32-bit Identity Root value
     * 
     * @param encRoot
     *            String 32-bit EnEncryption Root value
     * 
     * @return boolean Return true if merging is successful
     */
    private byte[] mergeKeys(byte[] bb, String bdaddr, int xtalfreqtrim, String idRoot, String encRoot) throws UnsupportedEncodingException, IncorrectImageException,StringIndexOutOfBoundsException {
        return CsConfig.mergeKeys(bb, bdaddr, xtalfreqtrim, idRoot, encRoot);
    }
    
    /**
     * Merge CS keys into updated image
     * 
     * @param data
     *            byte[] original image data
     * 
     *@param cskeyList
     *            List<CskeyEntry> The list of cskeys which are needed to merge into updated image
     * 
     * @return boolean Return true if merging is successful
     */
    private byte[] mergeKeys(byte[] bb, List<CskeyItem> cskeyList) throws UnsupportedEncodingException, IncorrectImageException,StringIndexOutOfBoundsException {
        return CsConfig.mergeKeys(bb, cskeyList);
    }
    
    /**
     * Check remote device if support reading CS Block, this is only in OTAU version 5
     * 
     * @return boolean Return true if remote device supports reading CS Block
     */
    public boolean getCsBlockSupported() {
        return mCsBlockSupported;
    }

    /**
     * Set boolean to indicate remote device support reading CS Block
     * 
     * @param isCsBlockSupported
     *            Boolean which indicate if remote device support reading CS Block
     */
    void setCsBlockSupported(boolean isCsBlockSupported) {
        this.mCsBlockSupported = isCsBlockSupported;
    }

    /**
     * Get indication if remote device support reading CS Key from Boot-loader Service
     * 
     * @return boolean Return true indicate if remote device support reading CS Keys from Boot-loader Service
     */
    public boolean getReadCskeyFromBootloader() {
        return mReadCskeyFromBootloader;
    }

    /**
     * Set boolean to indicate remote device support reading CS Key from Boot-loader Service
     * 
     * @param isSupported
     *            Boolean which indicate if remote device support reading CS Keys from Boot-loader Service
     */
    void setReadCskeyFromBootloader(boolean isSupported) {
        this.mReadCskeyFromBootloader = isSupported;
    }

    /**
     * Get indication if remote device support software version
     *
     * @return boolean Return true indicate if remote device support software version
     */
    boolean getCsrOtaVersion() {
        return mCsrOtaVersionSupported;
    }

    /**
     * Set boolean to indicate remote device support reading CS Key from Boot-loader Service
     *
     * @param isSupported
     *            Boolean which indicate if remote device support reading CS Keys from Boot-loader Service
     */
    void setCsrOtaVersion(boolean isSupported) {
        this.mCsrOtaVersionSupported = isSupported;
    }

    /**
     * Get indication if remote device support software version
     *
     * @return boolean Return true indicate if remote device support software version
     */
    public boolean getBootloaderSoftwareVersionSupported() {
        return mSoftwareVersionSupported;
    }
    /**
     * Set boolean to indicate remote device support bootloader software version
     *
     * @param exist
     *            Boolean which indicate if remote device support software version
     */
    void setReadBootloaderSoftwareVersion(boolean exist) {
        this.mSoftwareVersionSupported = exist;
    }

    /**
     * Set CS block offset and reading size
     * 
     * @param list
     *            List<?> PSkey entry which is read from uEnergy SDK csk_db.xml file
     */
    public void setCskeyList(ArrayList<CskeyItem> list) {
        mCskeyList = list;
    }
    
    /**
     * Get CS block offset and reading size
     * 
     * return ArrayList<CskeyItem> CSkey item which is read from uEnergy SDK csk_db.xml file
     */
    public ArrayList<CskeyItem> getCskeyList(){
        return mCskeyList;
    }
    
    /**
     * Add cskey item into merged ArrayList
     * 
     * @param cskey
     *            CskeyItem The CSkey which need to be merged into the image
     */
    public void addMergedCskeyItem(CskeyItem cskey){
        mMergedCsKeyList.add(cskey);
    }

    /**
     * Set a flag to indicate if attributes refresh is done
     *
     * @param done
     *            Boolean which indicate if refreshing attributes is done
     */
    public void setRefreshAttributesDone(boolean done) {
        mRefreshAttributesDone = done;
    }

    /**
     * Get the flag which indicates refreshing attributes is done
     *
     * @return the boolean which indicate refreshing attributes is done
     */
    public boolean getRefreshAttributesDone() {
        return mRefreshAttributesDone;
    }

    /**
     * Refresh local GATT attributes
     *
     */
    public void refreshAttribute() {
        if(mGattClient != null) {
            mGattClient.discoverService(true);
        }
    }

    /**
     * Set the device if it is a mesh device
     *
     * @param meshdevice
     *            Boolean if it is a mesh device
     */
    public void setIsMeshDevice(boolean meshdevice) {
        mIsMeshdevice = meshdevice;
    }

    /**
     * Check if this device is a mesh device
     *
     * @return if this is a mesh device
     */
    public boolean getIsMeshDevice() {
        return mIsMeshdevice;
    }
}
