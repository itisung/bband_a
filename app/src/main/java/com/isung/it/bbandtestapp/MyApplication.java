package com.isung.it.bbandtestapp;

import android.app.Application;
import android.bluetooth.BluetoothDevice;

/**
 * Created by leejs on 2015-12-10.
 */
public class MyApplication  extends Application
{
    private BluetoothDevice mGlobalBluetoothDevice;

    public BluetoothDevice getGlobalBluetoothDevice()
    {
        return mGlobalBluetoothDevice;
    }

    public void setGlobalBluetoothDevice(BluetoothDevice globalBluetoothDevice)
    {
        this.mGlobalBluetoothDevice = globalBluetoothDevice;
    }


}