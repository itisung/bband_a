/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

public class State {

    /**
     * The device connection state 
     * <li>{@link #STATE_INVALID}</li>
     * <li>{@link #STATE_INIT}</li>
     * <li>{@link #STATE_IDLE}</li>
     * <li>{@link #STATE_CONNECTING}</li>
     * <li>{@link #STATE_CONNECTED}</li>
     * <li>{@link #STATE_DISCONNECTING}</li>
     * <li>{@link #STATE_CANCEL_CONNECTING}</li>
     */
    public enum ConnectionState {

        /**
         * Invalid state
         */
        STATE_INVALID(0),
        /**
         * Init state
         */
        STATE_INIT(1),
        /**
         * Idle state
         */
        STATE_IDLE(2),
        /**
         * Connecting
         */
        STATE_CONNECTING(3),
        /**
         * Connected
         */
        STATE_CONNECTED(4),
        /**
         * Disconnecting
         */
        STATE_DISCONNECTING(5),
        /**
         * Cancel connecting
         */
        STATE_CANCEL_CONNECTING(6);

        private final int index;

        /**
         * The constructor for the enum type
         * 
         * @param index
         *            int the index of state
         */
        ConnectionState(int index) {
            this.index = index;
        }

        /**
         * Get the value of the enum variable
         * 
         * @return the value of state
         */
        public int valueOf() {
            return index;
        }
    };

    /**
     * The scan state 
     * <li>{@link #STATE_SCAN_IDLE}</li>
     * <li>{@link #STATE_SCAN_SCANNING}</li>
     * <li>{@link #STATE_SCAN_CANCEL}</li>
     */
    public enum ScanState {

        /**
         * Idle state
         */
        STATE_SCAN_IDLE(0),
        /**
         * Scanning state
         */
        STATE_SCAN_SCANNING(1),
        /**
         * Scan cancelled state
         */
        STATE_SCAN_CANCEL(2);

        private final int index;

        /**
         * The constructor for the enum type
         * 
         * @param index
         *            int the index of state
         */
        ScanState(int index) {
            this.index = index;
        }

        /**
         * Get the value of the enum variable
         * 
         * @return the value of state
         */
        public int valueOf() {
            return index;
        }
    };

    /**
     * The OTA module internal state
     * <li>{@link #STATE_OTA_INIT_READ_DATABASE}</li>
     * <li>{@link #STATE_OTA_INIT_READ_VERSION}</li>
     * <li>{@link #STATE_OTA_INIT_CONFIGURING_CCD}</li>
     * <li>{@link #STATE_OTA_INIT_READ_BT_ADDRESS}</li>
     * <li>{@link #STATE_OTA_INIT_READ_XTAL_TRIM}</li>
     * <li>{@link #STATE_OTA_INIT_READ_CHALLENGE}</li>
     * <li>{@link #STATE_OTA_INIT_READ_CURRENT_APP}</li>
     * <li>{@link #STATE_OTA_INIT_SET_CURRENT_APP}</li>
     * <li>{@link #STATE_OTA_DATA_TRANSFER}</li>
     * <li>{@link #STATE_OTA_PAUSE_DATA_TRANSFER}</li>
     * <li>{@link #STATE_OTA_RESET_TRANSFER_CTRL}</li>
     * <li>{@link #STATE_OTA_ABORT_DATA_TRANSFER}</li>
     * <li>{@link #STATE_OTA_SET_MODE}</li>
     * <li>{@link #STATE_OTA_SET_CURRENT_APP}</li>
     * <li>{@link #STATE_OTA_GET_CURRENT_APP}</li>
     * <li>{@link #STATE_OTA_WRITE_RESPONSE}</li>
     * <li>{@link #STATE_OTA_SET_TRANSFER_CTRL}</li>
     * <li>{@link #STATE_OTA_SET_TRANSFER_COMPLETE}</li>
     * <li>{@link #STATE_OTA_INIT_READ_IDENTITY_ROOT}</li>
     * <li>{@link #STATE_OTA_INIT_READ_ENCRYPTION_ROOT}</li>
     * <li>{@link #STATE_OTA_INIT_READ_BT_ADDRES_FROM_BL}</li>
     * <li>{@link #STATE_OTA_INIT_READ_XTAL_TRIM_FROM_BL}</li>
     * <li>{@link #STATE_OTA_INIT_READ_IDENTITY_ROOT_FROM_BL}</li>
     * <li>{@link #STATE_OTA_INIT_READ_ENCRPTION_APP_FROM_BL}</li>
     */
    public enum OtaState {

        /**
         * Initial state
         */
        STATE_OTA_IDLE(0x00),
        /**
         * Read the GATT database from the device
         */
        STATE_OTA_INIT_READ_DATABASE(0x1),
        /**
         * Read the specification version from the device
         */
        STATE_OTA_INIT_READ_VERSION(0x2),
        /**
         * Requesting notifications on all relevant characteristics
         */
        STATE_OTA_INIT_CONFIGURING_CCD(0x3),
        /**
         * Reading the device BT address
         */
        STATE_OTA_INIT_READ_BT_ADDRESS(0x4),
        /**
         * Reading the device crystal trim
         */
        STATE_OTA_INIT_READ_XTAL_TRIM(0x5),
        /**
         * Read the challenge value from the connected device
         */
        STATE_OTA_INIT_READ_CHALLENGE(0x6),
        /**
         * Reading the active application
         */
        STATE_OTA_INIT_READ_CURRENT_APP(0x7),
        /**
         * Setting the active application
         */
        STATE_OTA_INIT_SET_CURRENT_APP(0x8),
        /**
         * Image transfer in progress
         */
        STATE_OTA_DATA_TRANSFER(0x9),
        /**
         * Image transfer paused
         */
        STATE_OTA_PAUSE_DATA_TRANSFER(0x0a),
        /**
         * Image transfer reset
         */
        STATE_OTA_RESET_TRANSFER_CTRL(0x0b),
        /**
         * Image transfer aborted
         */
        STATE_OTA_ABORT_DATA_TRANSFER(0x0c),
        /**
         * Switching connected device into the boot-loader
         */ 
        STATE_OTA_SET_MODE(0x0d),
        /**
         * Setting the active application
         */
        STATE_OTA_SET_CURRENT_APP(0x0e),
        /**
         * Reading the active application
         */
        STATE_OTA_GET_CURRENT_APP(0x0f),
        /**
         * Writing the challenge response to the connected device
         */
        STATE_OTA_WRITE_RESPONSE(0x10),
        /**
         * Setting transfer control state to "in progress"
         */
        STATE_OTA_SET_TRANSFER_CTRL(0x11),
        /**
         * Setting transfer control state to complete
         */
        STATE_OTA_SET_TRANSFER_COMPLETE(0x12),
        /**
         * Reading identity root
         */
        STATE_OTA_INIT_READ_IDENTITY_ROOT(0x13),
        /**
         * Reading encryption root
         */
        STATE_OTA_INIT_READ_ENCRYPTION_ROOT(0x14),
        /**
         * Reading Bluetooth address from Bootloader service
         */
        STATE_OTA_INIT_READ_BT_ADDRES_FROM_BL(0x15),
        /**
         * Reading crystal trim from Bootloader service
         */
        STATE_OTA_INIT_READ_XTAL_TRIM_FROM_BL(0x16),
        /**
         * Reading identity root from Bootloader service
         */
        STATE_OTA_INIT_READ_IDENTITY_ROOT_FROM_BL(0x17),
        /**
         * Reading encryption root from Bootloader service
         */
        STATE_OTA_INIT_READ_ENCRPTION_APP_FROM_BL(0x18),
        /**
         * Start to read CS Block from on-chip Application Server
         */
        STATE_OTA_INIT_READ_CS_BLOCK(0x19),
        /**
         * Reading CS Block from on-chip Application Server
         */
        STATE_OTA_READ_CS_BLOCK(0x1a),
        /**
         * CS Block pend to read value from on-chip Application Server
         */
        STATE_OTA_READ_CS_BLOCK_PENDING(0x1b),
        /**
         * State for refreshing cached attribute
         */
        STATE_OTA_REFRESH_ATTRIBUTES(0x1c),
        /**
         * State for re-connecting GATT server
         */
        STATE_OTA_RECONNECT_GATT(0x1d);

        private final int index;

        /**
         * The constructor for the enum type
         * 
         * @param index
         */
        OtaState(int index) {
            this.index = index;
        }

        /**
         * Get the enum value
         * 
         * @return the value of the variable
         */
        public int valueOf() {
            return index;
        }
    };

    /**
     * The OTA module internal state
     * <li>{@link #READ_CS_BLOCK_BUILD_ID}</li>
     * <li>{@link #READ_CS_BLOCK_BT_ADDRESS}</li>
     * <li>{@link #READ_CS_BLOCK_XLST}</li>
     * <li>{@link #READ_CS_USER_KEY}</li>
     * <li>{@link #READ_CS_BLOCK_IDENTITY_ROOT}</li>
     * <li>{@link #READ_CS_BLOCK_ENCRYPTION_ROOT}</li>
     */
    public enum ReadCsBlockState {
        READ_CS_BLOCK_BUILD_ID(0),
        READ_CS_BLOCK_BT_ADDRESS(1),
        READ_CS_BLOCK_XLST(2),
        READ_CS_BLOCK_IDENTITY_ROOT(17),
        READ_CS_BLOCK_ENCRYPTION_ROOT(18);

        private int index;

        /**
         * The constructor for the enum type
         * 
         * @param index
         */
        ReadCsBlockState(int index) {
            this.index = index;
        }

        /**
         * Get the enum value
         * 
         * @return the value of the variable
         */
        public int valueOf() {
            return index;
        }

        /**
         * Get next state
         * 
         * @return the next state
         */
        public ReadCsBlockState getNext() {
            return this.ordinal() < ReadCsBlockState.values().length - 1 ? ReadCsBlockState.values()[this.ordinal() + 1]
                    : null;
        }
    };

    private static ConnectionState mConnectionState;
    private static ScanState mScanState;
    private static OtaState mOtaState;
    private static ReadCsBlockState mReadCsBlockState;

    /**
     * Set BLE connection state
     * 
     * @param newState
     *            ConnectionState the connection state
     */
    static void setState(ConnectionState newState) {
        mConnectionState = newState;
    }

    /**
     * Get BLE connection state
     * 
     * @return #ConnectionState the connection state
     */
    public static ConnectionState getState() {
        return mConnectionState;
    }

    /**
     * Set BLE Scanning state
     * 
     * @param newState
     *            #ScanState the connection state
     */
    static void setScanState(ScanState newState) {
        mScanState = newState;
    }

    /**
     * Get BLE Scanning state
     * 
     * @return #ScanState the scanning state
     */
    public static ScanState getScanState() {
        return mScanState;
    }

    /**
     * Set BLE OTA state
     * 
     * @param newState
     *            #OtaState OTA state
     */
    synchronized static void setOtaState(OtaState newState) {
        mOtaState = newState;
    }

    /**
     * Get OTA state
     * 
     * @return #OtaState OTA state
     */
    public synchronized static OtaState getOtaState() {
        return mOtaState;
    }

    /**
     * Set reading CS block state
     * 
     * @param newState
     *            #ReadCsBlockState reading CS Block state
     */
    static void setReadCsBlockState(ReadCsBlockState newState) {
        mReadCsBlockState = newState;
    }

    /**
     * Get reading cs block state
     * 
     * @return #ReadCsBlockState reading CS Block state
     */
    public static ReadCsBlockState getReadCsBlockState() {
        return mReadCsBlockState;
    }
}
