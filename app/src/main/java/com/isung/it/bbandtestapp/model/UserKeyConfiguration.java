/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp.model;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class UserKeyConfiguration {

    public final static int UpgradeMethodUnknown = 0;
    public final static int UpgradeMethod1 = 1;
    public final static int UpgradeMethod2 = 2;
    public final static int UpgradeMethod3 = 3;
    public final static int UpgradeMethod4 = 4;

    public final static int ConfirmationMethodUnknown = 0;
    public final static int ConfirmationMethod1 = 1;
    public final static int ConfirmationMethod2 = 2;
    public final static int ConfirmationMethod3 = 3;
    public final static int ConfirmationMethod4 = 4;

    private int mUpgradeBehaviour;
    private int mConfirmationMethod;

    /**
     * The constructor and initialise instance variable
     */
    public UserKeyConfiguration() {
        mUpgradeBehaviour = UpgradeMethodUnknown;
        mConfirmationMethod = ConfirmationMethodUnknown;
    }

    /**
     * Get upgrade behaviour in user keys configuration file
     * 
     * @return the index of upgrade behaviour
     */
    public int getUpgradeBehaviour() {
        return mUpgradeBehaviour;
    }

    /**
     * Set upgrade behaviour in user keys configuration file
     * 
     * @param behaviour
     *            int Upgrade Behaviour
     */
    public void setUpgradeBehaviour(int behaviour) {
        mUpgradeBehaviour = behaviour;
    }

    /**
     * Get confirmation method
     * 
     * @return the index of confirmation method
     */
    public int getConfirmationMethod() {
        return mConfirmationMethod;
    }

    /**
     * Set confirmation method
     * 
     * @param confirmation
     *            int Confirmation method
     */
    public void setConfirmationMethod(int confirmation) {
        mConfirmationMethod = confirmation;
    }

    /**
     * Load the configuration from the external storage
     * 
     * @param file
     *            String the user keys configuration file path
     */
    public boolean load(String file) {
        Log.d("JSLEE","load file = " + file);
        try {

            File configFile = new File(file);
            if (configFile.exists()) {
                SAXParserFactory spFactory = SAXParserFactory.newInstance();
                SAXParser parser = spFactory.newSAXParser();
                XMLReader xmlReader = parser.getXMLReader();

                ConfigXMLHandler configXmlHandler = new ConfigXMLHandler();
                xmlReader.setContentHandler(configXmlHandler);
                InputStream is = new FileInputStream(configFile);
                xmlReader.parse(new InputSource(is));
                Log.d("JSLEE","xmlReader.toString() = " + xmlReader.toString());

            }
            else {
                return false;
            }
        }
        catch (ParserConfigurationException pce) {
            Log.e("UserConfiguration", "sax parse error", pce);
            return false;
        }
        catch (SAXException se) {
            Log.e("UserConfiguration", "sax error", se);
            return false;
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Load the configuration from #InputStream
     * 
     * @param in
     *            InputStream the InpuStream objec reading from the raw folder
     */
    public boolean load(InputStream in) {
        try {

            if (in == null)
                return false;
            SAXParserFactory spFactory = SAXParserFactory.newInstance();
            SAXParser parser = spFactory.newSAXParser();
            XMLReader xmlReader = parser.getXMLReader();

            ConfigXMLHandler configXmlHandler = new ConfigXMLHandler();
            xmlReader.setContentHandler(configXmlHandler);
            xmlReader.parse(new InputSource(in));
            Log.d("JSLEE","xmlReader.toString() = " + xmlReader.toString());
        }
        catch (ParserConfigurationException pce) {
            Log.e("UserConfiguration", "sax parse error", pce);
            return false;
        }
        catch (SAXException se) {
            Log.e("UserConfiguration", "sax error", se);
            return false;
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * The class provides implementations for all of the callbacks in the SAX2 handler classes The XMl formatter like:
     * 
     * <remote_device_userkey> 
     *   <imagecache> 
     *     <item name="userkey0" value="4"/>
     *     <item name="userkey1" value="1"/>
     *     <item name="userkey2" value="1e12"/>
     *     <item name="userkey3" value="1e5a"/>
     *     <item name="userkey4" value="2"/>
     *     <item name="userkey5" value="0"/>
     *     <item name="userkey6" value="0"/>
     *     <item name="userkey7" value="0"/>
     *   </imagecache>
     * </remote_device_userkey>
     */
    public class ConfigXMLHandler extends DefaultHandler {
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

            if (localName.equals("item")) {
                if (attributes.getValue(0) != null) {
                    if (attributes.getValue(0).equalsIgnoreCase("userkey0")) {
                        try {
                            Log.d("JSLEE","userkey0 attributes.getValue(1) = " + attributes.getValue(1));
                            mUpgradeBehaviour = Integer.parseInt(attributes.getValue(1), 16);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                if (attributes.getValue(0) != null) {
                    if (attributes.getValue(0).equalsIgnoreCase("userkey1")) {
                        try {
                            mConfirmationMethod = Integer.parseInt(attributes.getValue(1), 16);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            super.endElement(uri, localName, qName);
        }

    }
}
