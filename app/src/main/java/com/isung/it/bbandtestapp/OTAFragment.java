/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/

package com.isung.it.bbandtestapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.csr.btsmart.BtSmartService;
import com.isung.it.bbandtestapp.model.CsKeyXmlParser;
import com.isung.it.bbandtestapp.model.CskeyItem;
import com.isung.it.bbandtestapp.model.IOtaMessageListener;
import com.isung.it.bbandtestapp.model.OtaUpdateManager;
import com.isung.it.bbandtestapp.model.State;
import com.isung.it.bbandtestapp.model.State.OtaState;
import com.isung.it.bbandtestapp.model.State.ReadCsBlockState;
import com.isung.it.bbandtestapp.model.UserKeyConfiguration;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * It provides the user interface to set OTA and download image file for a given BLE device The Activity communicates
 * with {@code BluetoothLeService}, which in turn interacts with the Bluetooth LE API.
 */

public class OTAFragment extends Fragment implements IOtaMessageListener {

    public static final String ARG_PLANET_NUMBER = "planet_number";
    public static final int MESSAGE_OTA_START = 0;
    public static final int MESSAGE_TIME_OUT = 1;
    private static String IMG_FILE = Environment.getExternalStorageDirectory().getPath() + "/csr/serial_server_update_7788.img";
    private static String IMG_PATH = Environment.getExternalStorageDirectory().getPath() + "/csr/";

    private BluetoothDevice mBleDevice = null;
    private DeviceActivity mContext = null;

    private View mRootView = null;
    private String mFilePath = null;
    private TextView mFileName;

    private TextView mDataField;
    private TextView mChallengeResponse;
    private Button mDownloadBtn;
    private Button mCancelDownloadBtn;
    private Button msetTargetBtn;

    private TextView mBtAddressTextView;
    private TextView mCrystalTimTextValue;
    private TextView mConnectionState;
    private TextView mSelecteBleDevice;
    private ScrollView mScrollView;
    private UserKeyConfiguration mUserConfig;
    private TextView mAppVersion;
    private LinearLayout mBtloaderVersionLayout;

    private ProgressBar mProgressBar;
    private TextView mProgressingval;

    private static final int MESSAGE_UNKNOWN = -1;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_SCAN_DEVICES = 2;
    private static final int REQUEST_CHOOSE_IMAGE_FILE = 3;

    private static final int DEFAULT_CS_VERSION = 0;
    private static final int MAX_CS_KEY_NUMBER = 100;

    private String mBtAddress;
    private int mCrystalTrim = -1;
    private String mIdentityRoot;
    private String mEncryptionRoot;
    private int mMessageLine = 0;
    private boolean mIsOTAUpdate = false;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case MESSAGE_OTA_START:
                    Log.d("JSLEE","checkOtaStart();");
                    checkOtaStart();
                    break;
                case MESSAGE_TIME_OUT:

                    break;
            }
        }
    };

    public OTAFragment() {
        // Empty constructor required for fragment subclasses
    }

    public static Fragment newInstance(int position) {
        Fragment fragment = new OTAFragment();
        Bundle args = new Bundle();
        args.putInt(OTAFragment.ARG_PLANET_NUMBER, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = (DeviceActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("JSLEE","onCreateView");
        mRootView = inflater.inflate(R.layout.ota_update_activity, container, false);
        int i = getArguments().getInt(ARG_PLANET_NUMBER);
        String planet = getResources().getStringArray(R.array.planets_array)[i];
        getActivity().getActionBar().setTitle(planet);

        final BluetoothManager bluetoothManager = (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager.getAdapter() == null || !bluetoothManager.getAdapter().isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        mProgressBar       = (ProgressBar)mRootView.findViewById(R.id.download_progress);
        mProgressingval    = (TextView)mRootView.findViewById(R.id.progress_indicator);

        // Sets up UI references.
        mDataField = (TextView) mRootView.findViewById(R.id.data_value);
        mConnectionState = (TextView) mRootView.findViewById(R.id.connection_state);

        mChallengeResponse = (TextView) mRootView.findViewById(R.id.challenge_response_value);
        mChallengeResponse.setText(R.string.null_value);

        mFileName = (TextView) mRootView.findViewById(R.id.img_file_name);
        mCrystalTimTextValue = (TextView) mRootView.findViewById(R.id.xlst_value);
        mScrollView = (ScrollView) mRootView.findViewById(R.id.textview_scrollview);
        mDownloadBtn = (Button) mRootView.findViewById(R.id.start_download);
        mDownloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOta();
            }
        });
        mCancelDownloadBtn = (Button) mRootView.findViewById(R.id.cancel_download);
        mCancelDownloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelOta();
            }
        });
        msetTargetBtn = (Button) mRootView.findViewById(R.id.set_target);
        msetTargetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanDevices();
            }
        });

        mSelecteBleDevice = (TextView)mRootView.findViewById(R.id.selected_ble_device);
        mAppVersion = (TextView) mRootView.findViewById(R.id.app_version);
        mBtloaderVersionLayout = (LinearLayout)mRootView.findViewById(R.id.bootload_version_layout);
        OtaUpdateManager.initialize(getActivity());
        mMessageLine = 0;

        initImageFile();

        //initBTConnect();

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mUserConfig = new UserKeyConfiguration();
        File file = new File(OtaUpdateManager.USER_KEYS_CONFIGURATION);
        if (file.exists()) {
            mUserConfig.load(OtaUpdateManager.USER_KEYS_CONFIGURATION);
        }
        else {
            mUserConfig.load(getResources().openRawResource(R.raw.userkey));
        }
        loadUserKeyConfiguration();

//        mFilePath = data.getStringExtra(ChooseImageFileDialog.RETURN_PATH_RESULT);
//        mFileName.setText(mFilePath);
    }


    public void onDestroy() {
        OtaUpdateManager.getInstance().destroy(getActivity());
        super.onDestroy();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//        case R.id.action_info:
//            if(getFragmentManager().findFragmentByTag(AppInfoFragment.class.getName()) == null){
//                Fragment fragment = Fragment.instantiate(this, AppInfoFragment.class.getName() );
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.add(android.R.id.content,fragment, AppInfoFragment.class.getName());
//                ft.addToBackStack(null);
//                ft.commit();
//            }
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    /**
     * Public function for scanning BLE devices
     *
     */
    private void scanDevices() {
        OtaUpdateManager.getInstance().disconnect();
        final Intent intent = new Intent(mContext, DeviceScanActivity.class);
        startActivityForResult(intent, REQUEST_SCAN_DEVICES);
    }

    /**
     * Display status info
     *
     * @param data
     *            String status data
     */
    private void showMessageLog(String data) {
        if (data != null) {
            try {
                boolean isScrolled = true;
                if(mMessageLine++ >= mDataField.getMaxLines()){
                    mDataField.setText("");
                    mMessageLine = 0;
                    isScrolled = false;
                }
                DateFormat df = DateFormat.getTimeInstance(DateFormat.DEFAULT, Locale.UK);
                String formattedDate = df.format(new Date());
                mDataField.append("<" + formattedDate + "> ");
                mDataField.append(data);
                mDataField.append("\n");
                if(isScrolled)
                    mScrollView.fullScroll(View.FOCUS_DOWN);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handle choose image file button clicked event
     *
     * @param view
     *         View The clicked button view
     */
    public void chooseFile(View view) {
//        Intent intent = new Intent(view.getContext(), ChooseImageFileDialog.class);
//        intent.putExtra(ChooseImageFileDialog.FILTER, ".img");
//        intent.putExtra(ChooseImageFileDialog.CURRENT_PATH, Environment.getExternalStorageDirectory().getPath() + "/csr/");
//        startActivityForResult(intent, REQUEST_CHOOSE_IMAGE_FILE);
    }

    private void initBTConnect(){
        showMessageLog("initBTConnect()");
        MyApplication myApp = (MyApplication) getActivity().getApplication();

        mBleDevice = myApp.getGlobalBluetoothDevice();
        if (mBleDevice != null){
            Log.d("JSLEE","mBleDevice != null");
            OtaUpdateManager.getInstance().setIsMeshDevice(false);
            mBtAddressTextView = ((TextView) mRootView.findViewById(R.id.device_address));
            mBtAddressTextView.setText(mBleDevice.getAddress());
            getActivity().getActionBar().setTitle(mBleDevice.getName());
            mDownloadBtn.setEnabled(true);
            mSelecteBleDevice.setText(mBleDevice.getName());
            mDataField.setText("");
        } else {
            Log.d("JSLEE","mBleDevice == null !!!!!!!!!!!!");
        }
    }

    private void initImageFile(){
        File f = new File(IMG_PATH);
        f.mkdir();

        BufferedOutputStream bufEcrivain;
        try {
            bufEcrivain = new BufferedOutputStream(
                    (new FileOutputStream(IMG_FILE)));
            BufferedInputStream VideoReader = new BufferedInputStream(
                    getResources().openRawResource(R.raw.serial_server_update_7788));
            byte[] buff = new byte[32 * 1024];
            int len;
            while ((len = VideoReader.read(buff)) > 0) {
                bufEcrivain.write(buff, 0, len);
            }
            bufEcrivain.flush();
            bufEcrivain.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        File imgFile = new File(IMG_FILE);
        if(imgFile != null){
            mFilePath = imgFile.getPath();
            if(mFileName != null){
                mFileName.setText(mFilePath);
            }
        }
    }

    public boolean isOTAUpdate(){
        return mIsOTAUpdate;
    }

    private void startOta() {
        Log.d("JSLEE","startOta() mFilePath = " + mFilePath + " mBleDevice = " + mBleDevice);

        mIsOTAUpdate = true;

        if(mFilePath == null || mBleDevice == null){
            showMessageLog(this.getResources().getString(R.string.choose_image_or_target_prompt));
            if(mDownloadBtn != null){
                mDownloadBtn.setEnabled(true);
            }

            mIsOTAUpdate = false;
            Log.d("JSLEE","if(mFilePath == null || mBleDevice == null){ return;");
            return;
        }

        //disconnect
        ((DeviceActivity)getActivity()).bleDisconnect();

        if(mHandler != null){
            Log.d("JSLEE","mHandler.sendEmptyMessageDelayed(MESSAGE_OTA_START, 500);");
            mHandler.sendEmptyMessageDelayed(MESSAGE_OTA_START, 500);
        }
    }

    private void checkOtaStart(){
        Log.d("JSLEE","checkOtaStart()");
        //연결 해지 확인후 동작
        if( ((DeviceActivity)getActivity()).isDeviceConnected() == true){
            Log.d("JSLEE","isDeviceConnected == true");
//            if(mDownloadBtn != null){
//                mDownloadBtn.setEnabled(true);
//            }

            showMessageLog("Check Ble Disconnect");
            if(mHandler != null){
                mHandler.sendEmptyMessageDelayed(MESSAGE_OTA_START, 500);
            }
            return;
        }

        Log.d("JSLEE","mBleDevice = " + mBleDevice);
        if(mBleDevice != null){
            Log.d("JSLEE","if(mBleDevice != null){");
            showMessageLog("checkOtaStart, connect");
            OtaUpdateManager.getInstance().connect(mBleDevice, this);
            mDownloadBtn.setEnabled(false);
        } else {
            mIsOTAUpdate = false;
        }
    }
    private void cancelOta(){
        Log.d("JSLEE","cancelOta()");
        mIsOTAUpdate = false;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                OtaUpdateManager.getInstance().cancelDownload();
            }
        });
    }

    /**
     * Set which application area will be updated in remote device 
     *
     */
    private void setApplicaitonId(){
        if (mUserConfig.getUpgradeBehaviour() == UserKeyConfiguration.UpgradeMethod1) {
            OtaUpdateManager.getInstance().setApplicationOnChip(1);
        }
        else if(mUserConfig.getUpgradeBehaviour() == UserKeyConfiguration.UpgradeMethod4) {
            OtaUpdateManager.getInstance().setApplicationOnChip(1);
        }
    }

    /**
     * Open OTAU UI Fragment 
     *
     */
    private void showOtaProgressbar(){
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressingval.setVisibility(View.VISIBLE);
        mProgressBar.setProgress(0);
        mProgressBar.setMax(100);
    }

    /**
     * Close OTAU UI Fragment 
     *
     */
    private void hideOtaProgressbar(){
        mProgressBar.setVisibility(View.INVISIBLE);
        mProgressingval.setVisibility(View.INVISIBLE);
        mDownloadBtn.setEnabled(false);
        mCancelDownloadBtn.setEnabled(false);
        mCancelDownloadBtn.setVisibility(View.GONE);
        mBtloaderVersionLayout.setVisibility(View.GONE);
    }

    /**
     * Handle set target button pressed event
     * * @param v
     *         View The clicked button view
     */
    public void setTarget(View v){
        scanDevices();
    }

    /**
     * Handle results returned from choosing image or selecting the target
     */
    @Override
    public synchronized void onActivityResult(final int requestCode, int resultCode, final Intent data) {

        Log.d("JSLEE","onActivityResult data = " + data);
//        if (requestCode == REQUEST_CHOOSE_IMAGE_FILE) {
//            if (resultCode == Activity.RESULT_OK) {
//                mFilePath = data.getStringExtra(ChooseImageFileDialog.RETURN_PATH_RESULT);
//                mFileName.setText(mFilePath);
//            }
//
//        }
//        else if (requestCode == REQUEST_SCAN_DEVICES) {
        if (requestCode == REQUEST_SCAN_DEVICES) {
            if (data == null)
                return;
            mBleDevice = data.getExtras().getParcelable(BluetoothDevice.EXTRA_DEVICE);
            boolean isMeshDevice = data.getExtras().getBoolean(DeviceScanActivity.MESH_DEVICE);
            OtaUpdateManager.getInstance().setIsMeshDevice(isMeshDevice);
            mBtAddressTextView = ((TextView) mContext.findViewById(R.id.device_address));
            Log.d("JSLEE","mBleDevice.getAddress() = " + mBleDevice.getAddress());
            mBtAddressTextView.setText(mBleDevice.getAddress());
            mContext.getActionBar().setTitle(mBleDevice.getName());
            if (resultCode == Activity.RESULT_OK) {
                mDownloadBtn.setEnabled(true);
                mSelecteBleDevice.setText(mBleDevice.getName());
                mDataField.setText("");
            } else {
                if (mBleDevice == null) {
                    mSelecteBleDevice.setText("");
                }
            }
        }
//        } else if(requestCode == REQUEST_ENABLE_BT ) {
//            if (resultCode != Activity.RESULT_OK) {
//                mDownloadBtn.setEnabled(false);
//            }
//        }
    }

    /**
     * Start downloading image
     *
     */
    private void startDownloading() {
        if (mFilePath == null)
            return;

        if (mBtAddress == null || mCrystalTrim == -1) {
            showMessageLog(getResources().getString(R.string.null_btaddress_xlst));
            return;
        }
        new MergeCsKeysToImage().execute(mFilePath);
    }


    /**
     * It performs background operations and publish results on the UI thread without having to manipulate threads and/or handlers.
     * Merging CS keys will take up UI thread's time slot and memory, so move to background thread.
     *
     */
    private class MergeCsKeysToImage extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... filePath) {
            File imgFile = new File(filePath[0]);
            int size = (int) imgFile.length();
            final byte[] data = new byte[size];
            boolean rtn = false;
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(imgFile));
                buf.read(data, 0, data.length);
                rtn = OtaUpdateManager.getInstance().setImageData(data, mBtAddress, mCrystalTrim, mIdentityRoot, mEncryptionRoot);
                buf.close();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
                rtn = false;
            }
            catch (IOException e) {
                e.printStackTrace();
                rtn = false;
            }
            return rtn;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                showOtaProgressbar();
                OtaUpdateManager.getInstance().sendNextImageChunk();
            }
            else {
                hideOtaProgressbar();
                showMessageLog(getResources().getString(R.string.merging_image_cs_key_exception));
            }
        }
    }

    /**
     * OTA update callback API implementation follows
     */

    @Override
    public void onBtAddressUpdate(String addres) {
        mBtAddress = addres;
        mBtAddressTextView.setText(addres);
        showMessageLog(getResources().getString(R.string.ble_read_btaddress)
                + getResources().getString(R.string.ble_ota_success));
    }

    @Override
    public void onCrystalTrimUpdate(final int value) {
        mCrystalTrim = value;
        mCrystalTimTextValue.setText(String.format("0x%02x", value));
        showMessageLog(getResources().getString(R.string.ble_read_xlst)
                + getResources().getString(R.string.ble_ota_success));
    }

    @Override
    public void onIdentityRootUpdate(String value) {
        mIdentityRoot = value;
        showMessageLog(getResources().getString(R.string.ble_identity_root_value) + value);
    }

    @Override
    public void onEncryptionRootUpdate(String value) {
        mEncryptionRoot = value;
        showMessageLog(getResources().getString(R.string.ble_encryption_root_value) + value);
    }

    @Override
    public void onBootloaderVersionUpdate(final String version) {
        mCancelDownloadBtn.setEnabled(true);
//        mCancelDownloadBtn.setVisibility(View.VISIBLE); //캔슬 버튼 제거
        mBtloaderVersionLayout.setVisibility(View.VISIBLE);
        if(mRootView != null){
            TextView btVersion = (TextView)mRootView.findViewById(R.id.bootload_version);
            btVersion.setText(version);
        }
    }

    @Override
    public void onCsrOtaVersionUpdate(String version) {
        showMessageLog(getResources().getString(R.string.ble_csr_ota_version_desc)
                + getResources().getString(R.string.ble_ota_success));
        try{
            int ver = Integer.parseInt(version);

            if(ver > 4)
            {
                getActivity().runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        OtaUpdateManager.getInstance().readNextCsBlock();
                    }
                });
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSoftwareVersionUpdate(String version) {
        showMessageLog(getResources().getString(R.string.btloader_software_version) + version);
    }

    @Override
    public void onApplicationVersionUpdate(String version) {
        mAppVersion.setText(version);
        showMessageLog(getResources().getString(R.string.ble_csr_ota_version_desc)
                + getResources().getString(R.string.ble_ota_success));
    }

    @Override
    public void onOtaMessageUpdate(int errorMessageId) {
        //String message = "Error id:=" + errorMessageId;
        String message = null;
        switch (errorMessageId) {
            case OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_BTADDRESS:
                message = this.getString(R.string.error_message_id_readcskey_btaddress);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_XLST:
                message = this.getString(R.string.error_message_id_readcskey_xlst);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_IDROOT:
                message = this.getString(R.string.error_message_id_readcskey_idroot);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_READCSKEY_ENCROOT:
                message = this.getString(R.string.error_message_id_readcskey_encroot);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_CHALLENGE_RESPONSE:
                message = this.getString(R.string.error_message_id_challenge_response);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_ENABLE_OTA:
                message = this.getString(R.string.error_message_id_enable_ota);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_WRITE_TO_TARGET:
                message = this.getString(R.string.error_message_id_write_to_target);
                getActivity().runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        hideOtaProgressbar();
                    }
                });
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_BL_TRANSFERCONTROL_NOTIFICATION:
                message = this.getString(R.string.error_message_id_bl_transfercontrol_notification);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_NULL_CHARACTERISTIC:
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_NULL_SERVICE:
                message = this.getString(R.string.error_message_id_null_service);
                break;
            case OtaUpdateManager.ERROR_MESSAGE_ID_SET_APPLICATION_ID:
                message = this.getString(R.string.error_message_id_set_application_id);
                break;
            default:
                break;
        }
        if(message != null)
            showMessageLog(message);
    }

    @Override
    public void onConnectionStateUpdate(final int state) {
        if (state == BtSmartService.MESSAGE_CONNECTED) {
            mConnectionState.setText(R.string.connected);
            showMessageLog(mContext.getString(R.string.gatt_connected_desc));
            mCSVersion = 0 ;

            if(mDownloadBtn != null){
                mDownloadBtn.setEnabled(false);
            }
        }
        else if (state == BtSmartService.MESSAGE_DISCONNECTED) {
            hideOtaProgressbar();
            mConnectionState.setText(R.string.disconnected);
            showMessageLog( mContext.getString(R.string.menu_disconnect));
            mCSVersion = 0 ;

            if(mDownloadBtn != null){
                mDownloadBtn.setEnabled(true);
            }
        }
        else if (state == MESSAGE_UNKNOWN) {
            mConnectionState.setText(R.string.disconnected);
            mDownloadBtn.setEnabled(false);
            mCancelDownloadBtn.setEnabled(false);
            mCancelDownloadBtn.setVisibility(View.GONE);
            mBtloaderVersionLayout.setVisibility(View.GONE);
            showMessageLog(mContext.getString(R.string.ble_not_support_ota_feature));
            mCSVersion = 0 ;
        }
    }

    @Override
    public void onOtaStateUpdate(final OtaState state) {
        switch (state) {
            case STATE_OTA_INIT_READ_BT_ADDRESS:
                showMessageLog(getResources().getString(R.string.ble_read_btaddress));
                break;
            case STATE_OTA_INIT_READ_XTAL_TRIM:
                showMessageLog(getResources().getString(R.string.ble_read_xlst));
                break;
            case STATE_OTA_SET_MODE:
                showMessageLog(getResources().getString(R.string.ble_boot_loader_mode));
                break;
            case STATE_OTA_INIT_READ_IDENTITY_ROOT:
                showMessageLog(getResources().getString(R.string.ble_read_identity_root));
                break;
            case STATE_OTA_INIT_READ_ENCRYPTION_ROOT:
                showMessageLog(getResources().getString(R.string.ble_read_encryption_root));
                // this is for V4 bootloader
                mDownloadBtn.setEnabled(true);
                showMessageLog(getResources().getString(R.string.ota_started));
                break;
            case STATE_OTA_SET_CURRENT_APP:
                showMessageLog(getResources().getString(R.string.ble_set_onchip_application_desc));
                break;
            case STATE_OTA_SET_TRANSFER_CTRL:
                showMessageLog(getResources().getString(R.string.ble_downloading_desc));
                startDownloading();
                break;
            case STATE_OTA_SET_TRANSFER_COMPLETE:
                Log.d("JSLEE","STATE_OTA_SET_TRANSFER_COMPLETE");
                hideOtaProgressbar();
                showMessageLog(getResources().getString(R.string.ble_download_complete_desc));
                break;
            case STATE_OTA_PAUSE_DATA_TRANSFER:
                showMessageLog(getResources().getString(R.string.ble_download_paused_desc));
                break;
            case STATE_OTA_ABORT_DATA_TRANSFER:
                Log.d("JSLEE","STATE_OTA_ABORT_DATA_TRANSFER");
                hideOtaProgressbar();
                showMessageLog(getResources().getString(R.string.ble_download_cancelled_desc));
                break;
            case STATE_OTA_INIT_READ_CHALLENGE:
                showMessageLog(getResources().getString(R.string.ble_read_challenge_key));
                break;
            case STATE_OTA_WRITE_RESPONSE:
                showMessageLog(getResources().getString(R.string.ble_respond_challenge_key));
                break;
            case STATE_OTA_READ_CS_BLOCK:
                showMessageLog(getResources().getString(R.string.ble_read_cs_block));
                break;
            case STATE_OTA_REFRESH_ATTRIBUTES:
                break;
            case STATE_OTA_RECONNECT_GATT:
                showMessageLog(getResources().getString(R.string.reconnect_gatt));
                break;
            default:
                break;
        }
    }

    @Override
    public void onOtaProgressUpdate(final int progress, final int total, final int sentNumber) {
        mProgressBar.setProgress(progress);
        mProgressingval.setText(String.format("%d", progress)+ "%," + String.format("total=%d,sent=%d bytes",total,sentNumber));

        if(progress >= 99 || total == sentNumber){
            Log.d("JSLEE","onOtaProgressUpdate 100 mIsOTAUpdate == true" + progress);
            mIsOTAUpdate = false;
            mContext.showUpdateComplitDialog();
        }
    }

    @Override
    public void onBondStateUpdate(final int state) {
        if (state == BtSmartService.MESSAGE_DEVICE_BONDED){
            showMessageLog(getResources().getString(R.string.ble_bonded_state_desc));
        } else if (state == BtSmartService.MESSAGE_DEVICE_BONDING) {
            showMessageLog(getResources().getString(R.string.ble_bonding_desc));
        }
        else {
            showMessageLog(getResources().getString(R.string.ble_nobond_desc));
        }
    }

    @Override
    public void onOtaTransferControlStatus(short state) {
        switch (state) {
            case OtaUpdateManager.SET_TRANSFER_CONTROL_INIT:
                OtaUpdateManager manager = OtaUpdateManager.getInstance();
                if(manager.getBootloaderSoftwareVersionSupported()) {
                    mChallengeResponse.setText(R.string.challenge_response_enabled);
                    manager.readDataTransferCharacteristic();
                }
                break;
            case OtaUpdateManager.SET_TRANSFER_CONTROL_READY:
                // handle V6 onwards
                manager = OtaUpdateManager.getInstance();
                if(manager.getBootloaderSoftwareVersionSupported())
                {
                    mChallengeResponse.setText(R.string.challenge_response_disabled);
                    manager.registerBootloaderTransferControlNotifiocation();
                    manager.readBootloadVersion();
                    manager.readBootloaderSoftwareVersion();

                    // This process is from V5 onwards
                    if (manager.getReadCskeyFromBootloader()) {

                        manager.readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_BT_FROM_BL,
                                OtaUpdateManager.USER_KEY_BT_ADDRESS);
                        manager.readDataTransferCharacteristic();
                        manager.readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_XLST_FROM_BL,
                                OtaUpdateManager.USER_KEY_XTAL_TRIM);
                        manager.readDataTransferCharacteristic();
                        manager.readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_IDROOT_FROM_BL,
                                OtaUpdateManager.USER_KEY_IDENTITY_ROOT);
                        manager.readDataTransferCharacteristic();
                        manager.readCSKeysFromBootloaderService(OtaUpdateManager.REQUEST_READ_CSKEYS_ENCROOT_FROM_BL,
                                OtaUpdateManager.USER_KEY_ENCRYPTION_ROOT);
                        manager.readDataTransferCharacteristic();
                    }

                    onOtauStarted();
                }
                break;
            case OtaUpdateManager.REQUEST_SET_TRANSFER_CONTROL_INPROGRESS:
                showMessageLog(getResources().getString(R.string.bl_contransfer_control_inprogress));
                break;
            case OtaUpdateManager.SET_TRANSFER_PAUSED:
                showMessageLog(getResources().getString(R.string.bl_contransfer_control_paused));
                break;
            case OtaUpdateManager.SET_TRANSFER_COMPLETED:
                showMessageLog(getResources().getString(R.string.bl_contransfer_control_completed));
                break;
            case OtaUpdateManager.SET_TRANSFER_FAILED:
                showMessageLog(getResources().getString(R.string.bl_contransfer_control_failed));
                break;
            case OtaUpdateManager.SET_TRANSFER_ABORT:
                showMessageLog(getResources().getString(R.string.bl_contransfer_control_aborted));
                break;
            default:
                showMessageLog(getResources().getString(R.string.bl_contransfer_control_unknown));
                break;
        }
    }

    private int mCSVersion = 0;
    @Override
    public void onCsBlockData(byte[] data) {

        if (data == null || data.length == 0 || data.length == 1)
            return;

        ReadCsBlockState state = State.getReadCsBlockState();
        if (state == null || (state == ReadCsBlockState.READ_CS_BLOCK_BUILD_ID && mCSVersion < (data[1] << 8 | data[0]) && mCSVersion != 0))
            return;

        switch (state) {
            case READ_CS_BLOCK_BUILD_ID:
                mCSVersion = data[1] << 8 | data[0];
                new LoadCskeysFromXmlFileTask().execute(mCSVersion);
                return;
            case READ_CS_BLOCK_BT_ADDRESS:
                String address = "";
                for (int i = data.length - 1; i >= 0; i--) {
                    address += String.format("%02X:", data[i]);
                }
                String btAddress = address.substring(0, address.length() - 1);
                mBtAddress = btAddress;
                mBtAddressTextView.setText(btAddress);
                setMergedCskeyValue(ReadCsBlockState.READ_CS_BLOCK_BT_ADDRESS.valueOf(),data);
                break;
            case READ_CS_BLOCK_XLST:
                mCrystalTrim = data[1] << 8 | data[0];
                mCrystalTimTextValue.setText(String.format("0x%02x", mCrystalTrim));
                setMergedCskeyValue(ReadCsBlockState.READ_CS_BLOCK_XLST.valueOf(),data);
                break;
            case READ_CS_BLOCK_IDENTITY_ROOT:
                String idRoot = "";
                for (byte bb : data) {
                    idRoot += String.format("%02x", bb);
                }
                mIdentityRoot = idRoot;
                setMergedCskeyValue(ReadCsBlockState.READ_CS_BLOCK_IDENTITY_ROOT.valueOf(),data);
                break;
            case READ_CS_BLOCK_ENCRYPTION_ROOT:
                String encRoot = "";
                for (byte bb : data) {
                    encRoot += String.format("%02x", bb);
                }
                mEncryptionRoot = encRoot;
                setMergedCskeyValue(ReadCsBlockState.READ_CS_BLOCK_ENCRYPTION_ROOT.valueOf(),data);
                break;
            default:
                showMessageLog(getResources().getString(R.string.invalid_csblock_id));
                break;
        }

        getActivity().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                OtaUpdateManager.getInstance().setNextReadCsBlockState();
                OtaUpdateManager.getInstance().readNextCsBlock();
            }
        });
    }

    @Override
    public void onOtauEnabled(boolean enabled) {
        OtaUpdateManager.getInstance().enableOTAMode();
        showMessageLog(getResources().getString(R.string.ota_started));
    }

    @Override
    public void onOtauStarted(){
        getActivity().runOnUiThread(new Runnable(){
            @Override
            public void run() {
                setApplicaitonId();
                OtaUpdateManager.getInstance().setTransferControlInProgress();
            }
        });

    }

    /**
     * Loading cs keys xml file into memory, it run on background thread
     *
     */
    private class LoadCskeysFromXmlFileTask extends AsyncTask<Integer, Integer, Boolean> {

        private String mXmlFilepath;
        @Override
        protected Boolean doInBackground(Integer... buildId) {
            Log.w("OTAUpdateActivity", "Cskey build id is:" + buildId[0]);
            if (buildId[0] <= 0 || buildId[0] >= MAX_CS_KEY_NUMBER) {
                Log.e("OTAUpdateActivity", "Cskey build id is not right," + buildId[0]);
                return false;
            }

            File file = new File(CsKeyXmlParser.CSKEY_XML_FILE);
            InputStream is = null;
            try {
                CsKeyXmlParser parser = new CsKeyXmlParser();
                mXmlFilepath = getResources().getString(R.string.default_cskey_db_xml);
                ArrayList<CskeyItem> cskeyList = null;
                if (file.exists()) {
                    is = new FileInputStream(file);
                    cskeyList = (ArrayList<CskeyItem>) parser.parse(is, buildId[0]);
                    mXmlFilepath = file.getPath();
                }
                else {
                    is = getResources().openRawResource(R.raw.cskey_db);
                    cskeyList = (ArrayList<CskeyItem>) parser.parse(is, DEFAULT_CS_VERSION);
                }

                is.close();
                is = null;

                if (cskeyList != null && cskeyList.size() > 0) {
                    OtaUpdateManager.getInstance().setCskeyList(cskeyList);
                }
                else {
                    return false;
                }
            }
            catch (FileNotFoundException e) {
                try {
                    if (is != null)
                        is.close();
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                    return false;
                }
                e.printStackTrace();
                return false;
            }
            catch (XmlPullParserException e) {
                try {
                    if (is != null)
                        is.close();
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                e.printStackTrace();
                return false;
            }
            catch (IOException e) {
                try {
                    if(is != null)
                        is.close();
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                showMessageLog(getResources().getString(R.string.loading_cskey_db_xml)+":"+mXmlFilepath);
                OtaUpdateManager.getInstance().setNextReadCsBlockState();
                OtaUpdateManager.getInstance().readNextCsBlock();
            } else {
                Log.w("OTAFragment",getResources().getString(R.string.cskey_db_xml_io_error));
            }
        }

    }

    /**
     * Set confirmation method and upgrade behaviour according by user keys configuration
     *
     */
    private void loadUserKeyConfiguration() {
        if (mUserConfig.getConfirmationMethod() == UserKeyConfiguration.ConfirmationMethod2
                || mUserConfig.getConfirmationMethod() == UserKeyConfiguration.ConfirmationMethod4) {
            mChallengeResponse.setText(R.string.challenge_response_enabled);
            OtaUpdateManager.getInstance().setHostValidation(true);
        }
        else {
            mChallengeResponse.setText(R.string.challenge_response_disabled);
            OtaUpdateManager.getInstance().setHostValidation(false);
        }
    }

    /**
     * Set specific CS key item into array list which will be used to merge 
     *
     *  @param cskeyId
     *                int specific cskey id
     *  @param data
     *                byte[] the value of CS keys
     */
    private void setMergedCskeyValue(int cskeyId,byte[] data){
        ArrayList<CskeyItem> cskeyList = OtaUpdateManager.getInstance().getCskeyList();
        if(cskeyList == null) return;

        try{
            for(CskeyItem entry:cskeyList){
                if(entry.id == cskeyId){
                    CskeyItem cskeyItem = new CskeyItem(entry.name,entry.id,entry.offset,entry.length);
                    System.arraycopy(data, 0, cskeyItem.value, 0, data.length);
                    OtaUpdateManager.getInstance().addMergedCskeyItem(cskeyItem);
                    Log.d("JSLEE","11 name = " + cskeyItem.name + " value = " + cskeyItem.value );

                    for(int a : cskeyItem.value){
                        Log.d("JSLEE","this.value = " + a);
                    }

                    break;
                }
            }
        } catch (Exception e) {
            Log.w("OTAFragment","Reading CS Block throw exception!");
        }

    }

}
